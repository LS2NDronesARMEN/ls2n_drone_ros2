# Useful plots for flight analysis
from tkinter import Tk
from tkinter import filedialog
from ls2n_tools.rosbag_extract import deserialize_rosbag
import matplotlib.pyplot as plt
import numpy as np


def plot_attitudes(flight_data):
    time = np.empty(0)
    x = np.empty(0)
    y = np.empty(0)
    z = np.empty(0)
    time_des = np.empty(0)
    x_des = np.empty(0)
    y_des = np.empty(0)
    z_des = np.empty(0)
    time_v = np.empty(0)
    x_v = np.empty(0)
    y_v = np.empty(0)
    z_v = np.empty(0)
    for timestamp, msg in (flight_data["/MocapSimulationDrone0/odom"]):
        time = np.append(time, timestamp)
        x = np.append(x, msg.pose.pose.orientation.x)
        y = np.append(y, msg.pose.pose.orientation.y)
        z = np.append(z, msg.pose.pose.orientation.z)
    for timestamp, msg in (flight_data["/Drone1/VehicleOdometry_PubSubTopic"]):
        time_v = np.append(time_v, timestamp)
        x_v = np.append(x_v, msg.q[1])
        y_v = np.append(y_v, -msg.q[2])
        z_v = np.append(z_v, -msg.q[3])
    for timestamp, msg in (flight_data["/Drone1/VehicleAttitudeSetpoint_PubSubTopic"]):
        time_des = np.append(time_des, timestamp)
        x_des = np.append(x_des, msg.q_d[1])
        y_des = np.append(y_des, -msg.q_d[2])
        z_des = np.append(z_des, -msg.q_d[3])
    plt.plot(time, x, 'r')
    plt.plot(time, y, 'b')
    plt.plot(time, z, 'g')
    plt.plot(time_v, x_v, 'r.-')
    plt.plot(time_v, y_v, 'b.-')
    plt.plot(time_v, z_v, 'g.-')
    plt.plot(time_des, x_des, 'r--')
    plt.plot(time_des, y_des, 'b--')
    plt.plot(time_des, z_des, 'g--')
    plt.show()


def main():
    root = Tk()
    root.withdraw()
    root.filename = filedialog.askopenfilename()
    flight_data = deserialize_rosbag(root.filename)
    root.destroy()
    plot_attitudes(flight_data)


if __name__ == 'main':
    main()
