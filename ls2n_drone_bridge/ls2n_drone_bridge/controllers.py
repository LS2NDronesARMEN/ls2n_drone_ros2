from px4_msgs.msg import *
from ls2n_drone_bridge.px4_com import OffboardMode
import transforms3d as tf3d
from math import *
import time
from ls2n_drone_bridge.common import *


def euler_from_quaternion(q):
    """
    Convert a quaternion into euler angles (roll, pitch, yaw)
    Uses the same computation as PX4 Firmware
    """
    a = q[0]
    b = q[1]
    c = q[2]
    d = q[3]
    aa = a * a
    ab = a * b
    ac = a * c
    ad = a * d
    bb = b * b
    bc = b * c
    bd = b * d
    cc = c * c
    cd = c * d
    dd = d * d
    dcm00 = aa + bb - cc - dd
    dcm02 = 2.0 * (ac + bd)
    dcm10 = 2.0 * (bc + ad)
    dcm12 = 2.0 * (cd - ab)
    dcm20 = 2.0 * (bd - ac)
    dcm21 = 2.0 * (ab + cd)
    dcm22 = aa - bb - cc + dd
    phi_val = atan2(dcm21, dcm22)
    theta_val = asin(-dcm20)
    psi_val = atan2(dcm10, dcm00)
    if abs(theta_val - pi / 2.0) < 1.0e-3:
        phi_val = 0.0
        psi_val = atan2(dcm12, dcm02)
    elif abs(theta_val + pi / 2.0) < 1.0e-3:
        phi_val = 0.0
        psi_val = atan2(-dcm12, -dcm02)
    return phi_val, theta_val, psi_val


# Those controller are additional layers build on the offboard attitude thrust control
class Controller:
    def __init__(self, node):
        self.node = node
        self.max_thrust = self.node.max_thrust
        self.type = ControllerType.NONE
    desired_state = FullState()
    desired_scaled_thrust = 0.0

    def set_attitude(self):
        setpoint = VehicleAttitudeSetpoint()
        setpoint.timestamp = round(time.monotonic_ns()/1.0e3)
        setpoint.q_d = self.desired_state.orientation.astype("float32")
        # Converting to PX4 frame
        setpoint.thrust_body = [0.0,
                                0.0,
                                -min(max(self.desired_scaled_thrust, 0.05), 1.0)]
        setpoint.q_d[2] = -setpoint.q_d[2]
        setpoint.q_d[3] = -setpoint.q_d[3]
        # This needs to be done in rtps as it was done by the mavlink receiver
        setpoint.roll_body, setpoint.pitch_body, setpoint.yaw_body = euler_from_quaternion(setpoint.q_d)
        self.node.px4_com.set_offboard_mode(OffboardMode.ATTITUDE_THRUST)
        self.node.px4_com.attitude_setpoint_publisher.publish(setpoint)


class PositionController(Controller):
    def __init__(self, node):
        super().__init__(node)
        self.pos_err_integral = 0
        self.desired_yaw = 0
        self.type = ControllerType.POSITION

    def update_trajectory(self, msg):
        for coordinate in msg.coordinates:
            if coordinate.name == "x":
                self.desired_state.position[0] = coordinate.position
                self.desired_state.velocity[0] = coordinate.velocity
                self.desired_state.acceleration[0] = coordinate.acceleration
            if coordinate.name == "y":
                self.desired_state.position[1] = coordinate.position
                self.desired_state.velocity[1] = coordinate.velocity
                self.desired_state.acceleration[1] = coordinate.acceleration
            if coordinate.name == "z":
                self.desired_state.position[2] = coordinate.position
                self.desired_state.velocity[2] = coordinate.velocity
                self.desired_state.acceleration[2] = coordinate.acceleration
            if coordinate.name == "yaw":
                self.desired_yaw = coordinate.position

    def do_control(self):
        # Position controller - stupid basic PID TO IMPROVE
        kp = self.node.get_parameter('kp').value
        kd = self.node.get_parameter('kd').value
        ki = self.node.get_parameter('ki').value
        error = self.desired_state.position - self.node.odometry.position
        error_d = self.desired_state.velocity - self.node.odometry.velocity
        feed_forward = self.desired_state.acceleration
        self.pos_err_integral += error*0.01
        desired_force = (feed_forward + kp*error + kd*error_d + ki*self.pos_err_integral)*self.node.mass/self.max_thrust
        desired_force += [0.0, 0.0, self.node.mass*9.81/self.max_thrust]  # Weight compensation
        # Computes desired orientation from desired force
        # We first build the rotation matrix then convert it to quaternion
        # Define the new frame - z is along the force
        z = desired_force/np.linalg.norm(desired_force)
        # x is uses the current rotation x value - will be corrected to the desired yaw in the end
        rotation = tf3d.quaternions.quat2mat(self.node.odometry.orientation)
        x = rotation[:, 0]
        y = np.cross(z, x)
        y = y/np.linalg.norm(y)
        x = np.cross(y, z)
        x = x/np.linalg.norm(x)
        new_rotation = np.transpose(np.array([x, y, z]))
        # Get the proper yaw
        roll, pitch, yaw = tf3d.euler.mat2euler(new_rotation, axes='rxyz')
        new_rotation = tf3d.euler.euler2mat(roll, pitch, self.desired_yaw, axes='rxyz')

        # Export desired thrust and orientation
        self.desired_scaled_thrust = np.linalg.norm(desired_force)
        self.desired_state.orientation = tf3d.quaternions.mat2quat(new_rotation)
        self.set_attitude()  # Calls the attitude thrust controller


class AttitudeThrustController(Controller):
    def __init__(self, node):
        super().__init__(node)
        self.type = ControllerType.ATTITUDE_THRUST

    def update_attitude_thrust(self, msg):
        self.desired_state.orientation = msg.attitude
        # Scale the thrust in 0 to 1
        fscale = msg.thrust / self.max_thrust
        self.desired_scaled_thrust = fscale
        self.set_attitude()

    def do_control(self):
        pass  # Nothing to do here. Attitude/thrust setpoints are directly passed through when received


class DirectMotorControl(Controller):
    def __init__(self, node):
        super().__init__(node)
        self.type = ControllerType.MOTOR_CONTROL

    def send_motor_control(self, msg):
        motor1 = min(max(msg.motor_velocity[0], 0.0), 1.0)
        motor2 = min(max(msg.motor_velocity[1], 0.0), 1.0)
        motor3 = min(max(msg.motor_velocity[2], 0.0), 1.0)
        motor4 = min(max(msg.motor_velocity[3], 0.0), 1.0)
        msg = ActuatorControls0()
        msg.timestamp_sample = round(time.monotonic_ns()/1.0e3)
        msg.timestamp = round(time.monotonic_ns()/1.0e3)
        # In PX4 we can control the motor through the mixer, so we need to inverse the mixer first
        msg.control[ActuatorControls0.INDEX_ROLL] = -0.5 * motor1 + 0.5 * motor2
        msg.control[ActuatorControls0.INDEX_PITCH] = 0.5 * motor3 - 0.5 * motor4
        msg.control[ActuatorControls0.INDEX_YAW] = 0.25 * motor1 + 0.25 * motor2 - 0.25 * motor3 - 0.25 * motor4
        msg.control[ActuatorControls0.INDEX_THROTTLE] = 0.25 * motor1 + 0.25 * motor2 + 0.25 * motor3 + 0.25 * motor4
        self.node.px4_com.set_offboard_mode(OffboardMode.MOTOR_CONTROL)
        self.node.px4_com.motors_publisher.publish(msg)

    def do_control(self):
        pass  # Nothing to do here. The motor control are directly passed through when received
