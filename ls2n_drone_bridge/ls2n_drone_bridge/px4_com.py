from px4_msgs.msg import *
from rclpy.qos import QoSProfile, qos_profile_sensor_data, DurabilityPolicy, ReliabilityPolicy
import time
from enum import IntEnum
qos_profile_sensor_data.depth = 1


class OffboardMode(IntEnum):
    MOTOR_CONTROL = 0,
    ATTITUDE_THRUST = 1


qos_profile_px4_com = QoSProfile(**{
    'history': qos_profile_sensor_data.history,
    'depth': qos_profile_sensor_data.depth,
    'reliability': ReliabilityPolicy.RELIABLE,
    'durability': DurabilityPolicy.TRANSIENT_LOCAL,
    'lifespan': qos_profile_sensor_data.lifespan,
    'deadline': qos_profile_sensor_data.deadline,
    'liveliness': qos_profile_sensor_data.liveliness,
    'liveliness_lease_duration': qos_profile_sensor_data.liveliness_lease_duration,
    'avoid_ros_namespace_conventions': qos_profile_sensor_data.avoid_ros_namespace_conventions
})


class Px4Comm:
    def __init__(self, node, target_system):
        self.node = node
        self.target_system = target_system
        # Vehicle control
        self.command_publisher = self.node.create_publisher(
            VehicleCommand,
            'VehicleCommand_PubSubTopic',
            qos_profile_px4_com
        )
        self.offboard_mode_publisher = self.node.create_publisher(
            OffboardControlMode,
            'OffboardControlMode_PubSubTopic',
            qos_profile_px4_com
        )
        self.attitude_setpoint_publisher = self.node.create_publisher(
            VehicleAttitudeSetpoint,
            'VehicleAttitudeSetpoint_PubSubTopic',
            qos_profile_px4_com
        )
        # Vehicle feedback
        self.node.create_subscription(
            VehicleControlMode,
            'VehicleControlMode_PubSubTopic',
            self.control_mode_listener_callback,
            qos_profile_px4_com
        )
        self.node.create_subscription(
            VehicleStatus,
            'VehicleStatus_PubSubTopic',
            self.status_listener_callback,
            qos_profile_px4_com
        )
        self.node.create_subscription(
            VehicleOdometry,
            'VehicleOdometry_PubSubTopic',
            self.node.main_loop,
            qos_profile_px4_com
        )
        self.motors_publisher = self.node.create_publisher(
            ActuatorControls0,
            'ActuatorControls0_PubSubTopic',
            qos_profile_px4_com
        )
        # MOCAP input
        self.mocap_publisher = self.node.create_publisher(
            VehicleVisualOdometry,
            'VehicleVisualOdometry_PubSubTopic',
            qos_profile_px4_com
        )
        self.confirmed_connection = False
        self.offboard = False

    def default_command(self):
        msg = VehicleCommand()
        msg.timestamp = round(time.monotonic_ns()/1.0e3)
        msg.target_system = self.target_system
        msg.target_component = 1
        msg.source_system = 255
        msg.from_external = True
        return msg

    def set_arm(self, arm):
        msg = self.default_command()
        msg.command = VehicleCommand.VEHICLE_CMD_COMPONENT_ARM_DISARM
        msg.param1 = 1.0 if arm else 0.0
        self.command_publisher.publish(msg)

    def set_offboard(self):
        msg = self.default_command()
        msg.command = VehicleCommand.VEHICLE_CMD_DO_SET_MODE
        msg.param1 = 1.0
        msg.param2 = 6.0
        self.command_publisher.publish(msg)

    def set_manual(self):
        msg = self.default_command()
        msg.command = VehicleCommand.VEHICLE_CMD_DO_SET_MODE
        msg.param1 = 1.0
        msg.param2 = 1.0
        self.command_publisher.publish(msg)

    def set_offboard_mode(self, mode):
        msg = OffboardControlMode()
        msg.timestamp = round(time.monotonic_ns()/1.0e3)
        msg.position = False
        msg.velocity = False
        msg.acceleration = False
        msg.attitude = False
        msg.body_rate = False
        msg.attitude = False
        msg.motors = False
        if mode == OffboardMode.ATTITUDE_THRUST:
            msg.attitude = True
        if mode == OffboardMode.MOTOR_CONTROL:
            msg.motors = True
        self.offboard_mode_publisher.publish(msg)

    def set_lockdown(self):
        msg = self.default_command()
        msg.command = VehicleCommand.VEHICLE_CMD_DO_FLIGHTTERMINATION
        msg.param1 = 2.0
        self.command_publisher.publish(msg)

    def release_lockdown(self):
        msg = self.default_command()
        msg.command = VehicleCommand.VEHICLE_CMD_DO_FLIGHTTERMINATION
        msg.param1 = 0.0
        self.command_publisher.publish(msg)

    # Vehicle feedback
    def control_mode_listener_callback(self, received_msg):
        self.node.update_armed(received_msg.flag_armed)
        self.offboard = received_msg.flag_control_offboard_enabled

    def status_listener_callback(self, received_msg):
        if not self.confirmed_connection:
            self.confirmed_connection = True
            self.node.get_logger().info("Connection to PX4 confirmed")
        # For security reason in experimentation, we for drone lockdown whenever we have a failsafe
        # this should be changed at term to handle proper failsafe modes in the PX4 firmware
        if received_msg.failsafe:
            self.node.emergency_stop()
