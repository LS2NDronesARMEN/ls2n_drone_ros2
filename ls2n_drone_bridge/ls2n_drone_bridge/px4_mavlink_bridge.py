import rclpy
from rclpy.node import Node
from pymavlink import mavutil
import time
from ls2n_interfaces.srv import DroneGainSet


class DroneMavlinkBridge(Node):
    def __init__(self):
        super().__init__('drone_mavlink_bridge')
        self.declare_parameter('connection_type', 'sitl')
        self.get_logger().info("Starting PX4 Mavlink bridge node")
        if self.get_parameter('connection_type').value == 'sitl':
            self.connection = mavutil.mavlink_connection('udpin:localhost:14540')
        elif self.get_parameter('connection_type').value == 'offboard':
            self.connection = mavutil.mavlink_connection('udpin:localhost:3801')
        else:
            self.get_logger().error("Invalid connection type")
            exit(1)
        self.get_logger().info("Waiting for mavlink heartbeat")
        self.connection.wait_heartbeat()
        self.get_logger().info("Connected to mavlink with system " + str(self.connection.target_system))
        self.create_service(DroneGainSet, "DroneGainSet", self.set_px4_parameter)

    def set_px4_parameter(self, request, response):
        param_name = request.name.upper()
        ack = None
        if param_name in [
            "MC_PITCHRATE_P",
            "MC_PITCHRATE_D",
            "MC_PITCHRATE_K",
            "MC_PITCHRATE_I",
            "MC_PITCH_P",
            "MC_ROLLRATE_P",
            "MC_ROLLRATE_D",
            "MC_ROLLRATE_K",
            "MC_ROLLRATE_I",
            "MC_ROLL_P",
            "MC_YAWRATE_P",
            "MC_YAWRATE_D",
            "MC_YAWRATE_K",
            "MC_YAWRATE_I",
            "MC_YAW_P"
        ]:
            retry = 0
            while retry < 5:
                if request.value != 666.0:
                    self.connection.param_set_send(param_name, request.value)
                else:
                    self.connection.param_fetch_one(param_name) # Allow to send 666 just to get the parameter value
                tstart = time.time()
                while time.time() - tstart < 1:
                    ack = self.connection.recv_match(type='PARAM_VALUE', blocking=True, timeout=0.1)
                    if ack is None:
                        continue
                    if param_name == str(ack.param_id).upper():
                        self.get_logger().info("Parameter " + param_name + " set to " + str(ack.param_value))
                        retry = 10
                        break
                retry += 1
            if ack is None:
                self.get_logger().error("Parameter " + param_name + " not set (acknowledge timeout)")
        else:
            self.get_logger().error("Invalid parameter")
        if ack is not None:
            response.value = ack.param_value
        return response


def main(args=None):
    rclpy.init(args=args)
    drone_bridge = DroneMavlinkBridge()
    try:
        rclpy.spin(drone_bridge)
    except KeyboardInterrupt:
        print('Shutting down drone mavlink bridge')
    finally:
        drone_bridge.destroy_node()
        rclpy.shutdown()


if __name__ == 'main':
    main()
