import rclpy
from rclpy.node import Node
from std_msgs.msg import Bool
from std_srvs.srv import Empty
from ls2n_drone_bridge.controllers import *
from ls2n_interfaces.srv import *
from ls2n_interfaces.msg import *
from ls2n_drone_bridge.px4_com import *
from rcl_interfaces.msg import ParameterDescriptor
from rcl_interfaces.msg import ParameterType
from nav_msgs.msg import Odometry


class Status:
    status = DroneStatus.IDLE
    lastAlive = None


class DroneRtpsBridge(Node):
    def __init__(self):
        super().__init__('drone_rtps_bridge')
        # Parameters
        mass_param_descriptor = ParameterDescriptor(type=ParameterType.PARAMETER_DOUBLE,
                                                    description='Drone mass')
        max_thrust_param_descriptor = ParameterDescriptor(type=ParameterType.PARAMETER_DOUBLE,
                                                          description='Drone maximal thrust')
        kp_param_descriptor = ParameterDescriptor(type=ParameterType.PARAMETER_DOUBLE,
                                                  description='Drone position controller P gain')
        kd_param_descriptor = ParameterDescriptor(type=ParameterType.PARAMETER_DOUBLE,
                                                  description='Drone position controller D gain')
        ki_param_descriptor = ParameterDescriptor(type=ParameterType.PARAMETER_DOUBLE,
                                                  description='Drone position controller I gain')
        direct_control_param_descriptor = ParameterDescriptor(
            type=ParameterType.PARAMETER_STRING,
            description='Direct control mode. none, position_control')
        self.declare_parameter('mass', 1.0, mass_param_descriptor)
        self.declare_parameter('max_thrust', 47.3, max_thrust_param_descriptor)
        self.declare_parameter('kp', 2.0, kp_param_descriptor)
        self.declare_parameter('kd', 2.0, kd_param_descriptor)
        self.declare_parameter('ki', 0.05, ki_param_descriptor)
        self.declare_parameter('direct_control_mode', 'none', direct_control_param_descriptor)
        self.mass = self.get_parameter('mass').value
        self.max_thrust = self.get_parameter('max_thrust').value
        self.drone_number = int(self.get_namespace()[-1])  # Get the drone number from namespace

        self.get_logger().info("Starting PX4 RTPS bridge node")
        self.get_logger().info("Drone mass " + str(self.mass) + " kg")
        self.get_logger().info("Drone max thrust " + str(self.max_thrust) + " N")
        # Keep alive
        self.keep_alive_timer = self.create_timer(0.05, self.keep_alive_check)
        self.keep_alive_subscription = self.create_subscription(
            Bool,
            'KeepAlive',
            self.keep_alive_callback,
            qos_profile_sensor_data
        )
        # Services
        self.create_service(
            StartControl,
            'StartControl',
            self.start_control
        )
        self.create_service(
            Empty,
            'SpinMotors',
            self.spin_motor
        )
        self.create_service(
            Empty,
            'LandDisarm',
            self.land_and_disarm
        )
        # Bridge feedback
        self.status_publisher = self.create_publisher(
            DroneStatus,
            'Status',
            qos_profile_sensor_data
        )
        self.odom_publisher = self.create_publisher(
            Odometry,
            'EKF/odom',
            qos_profile_sensor_data
        )
        self.status_timer = self.create_timer(0.01, self.publish_status)
        # MOCAP subscription
        self.mocap_subscription = self.create_subscription(
            Odometry,
            "Mocap/odom",
            self.send_mocap,
            qos_profile_sensor_data
        )
        # Set points
        self.create_subscription(
            MotorControlSetPoint,
            'MotorControlSetPoint',
            self.motors_set_point_callback,
            qos_profile_sensor_data
        )
        self.create_subscription(
            AttitudeThrustSetPoint,
            'AttitudeThrustSetPoint',
            self.attitude_thrust_set_point_callback,
            qos_profile_sensor_data)
        self.create_subscription(
            Trajectory,
            'Trajectory',
            self.trajectory_callback,
            qos_profile_sensor_data)
        # Other init
        self.px4_com = Px4Comm(self, self.drone_number)
        self.status.lastAlive = self.get_clock().now()
        self.emergency_counter = 0
        self.start_counter = 0
        self.land_counter = 0.0

    # Keep alive
    def keep_alive_callback(self, received_msg):
        if received_msg.data:
            self.status.lastAlive = self.get_clock().now()

    def keep_alive_check(self):
        if (self.get_clock().now() - self.status.lastAlive).nanoseconds > 1e8 and \
                (self.status.status == DroneStatus.ARMED or self.status.status == DroneStatus.FLYING):
            if not self.status.status == DroneStatus.EMERGENCY_STOP:
                self.get_logger().info("Keep alive lost,emergency shutdown")
                self.emergency_stop()

    # Bridge feedback
    def publish_status(self):
        msg = DroneStatus()
        msg.status = self.status.status
        self.status_publisher.publish(msg)

    # Drone feedback actions
    def update_armed(self, armed):
        if self.status.status == DroneStatus.PRE_ARMED and armed:
            self.get_logger().info("Drone Armed")
            self.status.status = DroneStatus.ARMED
        if (self.status.status == DroneStatus.FLYING or self.status.status == DroneStatus.ARMED) and not armed:
            self.get_logger().info("Disarmed by PX4")
            self.status.status = DroneStatus.IDLE
            if self.controller is not None:
                self.controller = None
            self.px4_com.set_manual()

    def update_states(self, odometry):
        # Transpose frames from PX4 representation (front, right, down) to our representation (front, left, up)
        self.odometry.position = np.array([odometry.x, -odometry.y, -odometry.z])
        self.odometry.orientation = np.array([odometry.q[0], odometry.q[1], -odometry.q[2], -odometry.q[3]])
        self.odometry.velocity = np.array([odometry.vx, -odometry.vy, -odometry.vz])
        self.odometry.angular_velocity = np.array([odometry.rollspeed, -odometry.pitchspeed, -odometry.yawspeed])
        # Broadcast odometry in reference frame (front, left, up)
        odom = Odometry()
        odom.pose.pose.position.x = odometry.x
        odom.pose.pose.position.y = -odometry.y
        odom.pose.pose.position.z = -odometry.z
        odom.pose.pose.orientation.w = float(odometry.q[0])
        odom.pose.pose.orientation.x = float(odometry.q[1])
        odom.pose.pose.orientation.y = float(-odometry.q[2])
        odom.pose.pose.orientation.z = float(-odometry.q[3])
        odom.twist.twist.linear.x = odometry.vx
        odom.twist.twist.linear.y = -odometry.vy
        odom.twist.twist.linear.z = -odometry.vz
        odom.twist.twist.angular.x = odometry.rollspeed
        odom.twist.twist.angular.y = -odometry.pitchspeed
        odom.twist.twist.angular.z = -odometry.yawspeed
        self.odom_publisher.publish(odom)

    # Requests from control center
    def spin_motor(self, request, response):
        self.get_logger().info("Spin motor resquest received")
        if self.status.status == DroneStatus.IDLE:
            self.start_counter = 0
            self.controller = Controller(self)
            self.controller.desired_scaled_thrust = 0.05
            self.controller.desired_state.orientation = self.odometry.orientation
            self.status.status = DroneStatus.PRE_ARMED
        else:
            self.get_logger().error("Request refused, drone not IDLE")
        return response

    def land_and_disarm(self, request, response):
        if self.status.status in {DroneStatus.ARMED, DroneStatus.PRE_ARMED}:
            self.normal_stop()
        if self.status.status in {DroneStatus.FLYING}:
            self.land_counter = 1000.0
            self.controller = Controller(self)
            self.controller.desired_scaled_thrust = self.mass*9.81/self.max_thrust*self.land_counter/1000.0
            self.controller.desired_state.orientation = np.array(([1.0, 0.0, 0.0, 0.0]))
            self.status.status = DroneStatus.LANDING
        return response

    def start_control(self, request, response):
        self.get_logger().info("Start control request received")
        if self.status.status == DroneStatus.ARMED:  # For now, it is not allowed to switch mode while flying
            if request.control_mode == StartControl.Request.ATTITUDE_THRUST:
                self.controller = AttitudeThrustController(self)
            elif request.control_mode == StartControl.Request.POSITION:
                self.controller = PositionController(self)
            elif request.control_mode == StartControl.Request.DIRECT_MOTOR_CONTROL:
                self.controller = DirectMotorControl(self)
            else:
                self.get_logger().error("Invalid control mode")
                response.ack = False
                return response
            self.status.status = DroneStatus.FLYING
            response.ack = True
        else:
            self.get_logger().error("Request refused, drone not in ARMED status")
            response.ack = False
        return response

    def send_mocap(self, msg_in):
        msg_out = VehicleVisualOdometry()
        msg_out.timestamp = round(time.monotonic_ns()/1.0e3)
        msg_out.timestamp_sample = round(time.monotonic_ns()/1.0e3)
        # Transform into the drone frame
        msg_out.x = msg_in.pose.pose.position.x
        msg_out.y = -msg_in.pose.pose.position.y
        msg_out.z = -msg_in.pose.pose.position.z
        msg_out.q = [msg_in.pose.pose.orientation.w,
                     msg_in.pose.pose.orientation.x,
                     -msg_in.pose.pose.orientation.y,
                     -msg_in.pose.pose.orientation.z]
        self.px4_com.mocap_publisher.publish(msg_out)

    # This loop is triggered at each odometry msg received
    def main_loop(self, msg):
        self.update_states(msg)
        if self.controller is not None and self.status.status == DroneStatus.FLYING:
            self.controller.do_control()
        # Arming procedure
        if self.controller is not None and self.status.status in {DroneStatus.PRE_ARMED, DroneStatus.ARMED}:
            self.controller.desired_scaled_thrust = 0.05
            self.controller.desired_state.orientation = self.odometry.orientation
            self.controller.set_attitude()
            if self.start_counter > 50 and not self.px4_com.offboard:
                self.px4_com.set_offboard()
            if self.start_counter > 100 and self.status.status == DroneStatus.PRE_ARMED:
                if self.px4_com.offboard:
                    self.px4_com.set_arm(True)
            if self.start_counter > 149 and self.status.status != DroneStatus.ARMED:
                self.get_logger().warn("Arming procedure failed")
                self.normal_stop()
            if self.start_counter < 151:
                self.start_counter += 1

        # Landing procedure
        if self.controller is not None and self.status.status == DroneStatus.LANDING:
            self.land_counter -= 1.0
            self.controller.desired_scaled_thrust = self.mass*9.81/self.max_thrust*self.land_counter/1000.0
            self.controller.desired_state.orientation = np.array(([1.0, 0.0, 0.0, 0.0]))
            self.controller.set_attitude()
            if self.land_counter < 100.0:
                self.normal_stop()

        if self.status.status == DroneStatus.EMERGENCY_STOP:
            if self.emergency_counter < 990:
                self.px4_com.set_lockdown()  # Ensures that the lockdown is sent to the drone while in emergency stop
            if self.emergency_counter > 989:
                self.px4_com.release_lockdown()
                self.px4_com.set_manual()
            if self.emergency_counter < 1000:  # Disable the drone for 10 sec in case of emergency stop
                self.emergency_counter += 1
            else:
                self.status.status = DroneStatus.IDLE
                self.get_logger().info("Recovered from emergency stop")

    # Set points
    def motors_set_point_callback(self, msg):
        if self.controller is not None:
            if self.controller.type == ControllerType.MOTOR_CONTROL:
                self.controller.send_motor_control(msg)

    def attitude_thrust_set_point_callback(self, msg):
        if self.controller is not None:
            if self.controller.type == ControllerType.ATTITUDE_THRUST:
                self.controller.update_attitude_thrust(msg)

    def trajectory_callback(self, msg):
        if self.controller is not None:
            if self.controller.type == ControllerType.POSITION:
                self.controller.update_trajectory(msg)

    # Others
    def emergency_stop(self):
        if self.status.status != DroneStatus.EMERGENCY_STOP:
            self.get_logger().warn("Drone Emergency Stop")
            self.status.status = DroneStatus.EMERGENCY_STOP
            self.emergency_counter = 0
            if self.controller is not None:
                self.controller = None
            self.px4_com.set_lockdown()

    def normal_stop(self):
        if self.status.status not in {DroneStatus.EMERGENCY_STOP, DroneStatus.IDLE}:
            self.get_logger().info("Stopping drone")
            self.status.status = DroneStatus.IDLE
            if self.controller is not None:
                self.controller = None
            self.px4_com.set_arm(False)
            self.px4_com.set_manual()

    status = Status()
    start_counter = 0
    controller = None
    odometry = FullState()


def main(args=None):
    rclpy.init(args=args)
    drone_bridge = DroneRtpsBridge()
    try:
        rclpy.spin(drone_bridge)
    except KeyboardInterrupt:
        print('Shutting down drone rtps bridge')
    finally:
        drone_bridge.destroy_node()
        rclpy.shutdown()


if __name__ == 'main':
    main()
