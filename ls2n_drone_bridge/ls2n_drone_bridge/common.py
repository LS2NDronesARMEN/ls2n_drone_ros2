import numpy as np
from enum import IntEnum


class FullState:
    position = np.array([0.0, 0.0, 0.0])
    velocity = np.array([0.0, 0.0, 0.0])
    acceleration = np.array([0.0, 0.0, 0.0])
    orientation = np.array(([1.0, 0.0, 0.0, 0.0]))
    angular_velocity = np.array([0.0, 0.0, 0.0])
    angular_acceleration = np.array([0.0, 0.0, 0.0])


class ControllerType(IntEnum):
    NONE = 0,
    MOTOR_CONTROL = 1,
    POSITION = 2,
    ATTITUDE_THRUST = 3