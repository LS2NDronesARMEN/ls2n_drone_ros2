[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

ls2n_drone_bridge
=====================
LS2N_drone_bridge is a ROS2 package to interact with the drones available at the LS2N laboratory.

Supported firmwares
--------------------------
PX4

Usage
---------------------------
This bridge can be used either:

- In simulation: Use the *ls2n_drones_simulation* package to launch your simulation environment.
- Offboard: Use the offboard_bridge.launch.py. Bridge setup must be done in config/offboard_params.yaml in this case.

The classic usage of this bridge is to use the *SpinMotors* service to start the motors and arm the drone. Then, start
the appropriate control mode using *StartControl* service. To use a given control, the appropriate input topic should be
used (i.e., *Trajectory* for position control, *AttitdeThrustSetPoint* for attitude_thrust, ...).

Topics
--------------------------
Topics ending with "PubSubTopic" are automatically generated from the rtps agent and are complicated to use as they are
directly translated as a UORB topic for the PX4 controller. It is thus recommended to not use those topics unless you
know exactly what you are doing. You may use following topics and services to interact with the bridge.

- *KeepAlive*: This topic ensures that communication is up with the bridge. Offboard mode is maintained only if the keep
  alive is up. Is no messages are received on this topic for more than 0.1 second, the drone goes in "emergency
  shutdown" mode.
- *Status*: Provides feedback about the drone status.
- *Trajectory*: To control the drone in position, you must feed this topic with the desired position. Now, only position
  is used but velocity and acceleration are available for future developments.
- *AttitudeThrustSetPoint*: Set points for attitude/thrust control. Attitude in quaternion and thrust from 0 to
  max_thrust.
- *MotorControlSetPoint*: Set points for direct motor control, scaled from 0 to 1.
- *Mocap/odom*: MOCAP informations to be sent to the drone.
- *EKF/odom*: Odometry computed by the drone (with internal EKF).

Services
----------------------------

- *SpinMotors*: Spin the drone motors. This should be done before switching to other control modes.
- *StartControl*: Start the flight with a given control mode or switch control mode in flight.
- *LandDisarm*: End the flight.

**For the PX4**, two bridges can be used (rtps, mavlink or both). Rtps bridge is the main bridge for setpoints and
feedback while mavlink bridge must be used for specific operations that are not available on the rtps bridge (parameter
settings for example). The use of both protocols at the same time in real experiments requires appropriate
configurations on PX4 side and the use of protocol splitter.
