from launch import LaunchDescription
from launch.actions import ExecuteProcess
from launch_ros.actions import Node
import os
import yaml
from ament_index_python.packages import get_package_share_directory
from datetime import datetime

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H:%M:%S")

config_file = os.path.join(
    get_package_share_directory('ls2n_drone_bridge'),
    'config',
    'offboard_params.yaml'
)
with open(config_file) as file:
    config = yaml.load(file, Loader=yaml.FullLoader)

drone_name = list(config.keys())[0]
log_folder = config[drone_name]["drone_rtps_bridge"]["log_folder"]


def generate_launch_description():
    if config[drone_name]['drone_rtps_bridge']['ros__parameters']['mavlink_bridge']:
        launch_commands = [
            ExecuteProcess(cmd=['protocol_splitter', '-b', '921600', '-d', '/dev/ttyS0',
                                '-y', '4800', '-z', '4801', '-w', '3800', '-x', '3801']),
            ExecuteProcess(cmd=['micrortps_agent', '-t', 'UDP', '-r', '4801', '-s', '4800', '-n', drone_name]),
            Node(
                package='ls2n_drone_bridge',
                executable='px4_mavlink_bridge',
                output='screen',
                namespace=drone_name,
                parameters=[{'connection_type': 'offboard'}]
            )]
    else:
        launch_commands = [
            ExecuteProcess(cmd=['micrortps_agent', '-b', '921600', '-d', '/dev/ttyS0', '-n', drone_name])
            ]

    launch_commands = launch_commands + [
        Node(package='ls2n_drone_bridge',
             executable='px4_rtps_bridge',
             output='screen',
             namespace=drone_name,
             parameters=[config_file]),
        ExecuteProcess(
            cmd=['ros2', 'bag', 'record', '-o', log_folder + "/" + dt_string,
                 '-e', '.*PubSubTopic$'],
            output='screen'
        )
    ]
    return LaunchDescription(launch_commands)
