from setuptools import setup
import os
from glob import glob

package_name = 'ls2n_drone_support'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name), glob('launch/*.launch.py')),
        (os.path.join('share', package_name, 'config'), glob('config/*.yaml')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Damien Six',
    maintainer_email='damien.six@ls2n.fr',
    description='Tools for LS2N drones test and calibration',
    license='Apache 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'thrust_estimation = ls2n_drone_support.thrust_estimation:main',
            'pid_tuning = ls2n_drone_support.pid_tuning:main',
            'position_pid_tuning = ls2n_drone_support.position_pid_tuning:main',
            'thrust_test = ls2n_drone_support.thrust_test:main',
        ],
    },
)
