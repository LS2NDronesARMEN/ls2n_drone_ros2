# Estimate the drone maximal thrust from flight data
from tkinter import Tk
from tkinter import filedialog
from ls2n_tools.rosbag_extract import deserialize_rosbag
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import SpanSelector
from scipy.interpolate import CubicSpline
from operator import itemgetter
import argparse


def thrust_estimation(flight_data, name):
    time_thrust = np.empty(0)
    thrust = np.empty(0)
    time_acc = np.empty(0)
    acceleration = np.empty(0)
    time_rotors = np.empty(0)
    rotors_vel_square_sum = np.empty(0)
    velocity_check = False

    for _, msg in (flight_data["/"+name+"/VehicleAttitudeSetpoint_PubSubTopic"]):
        time_thrust = np.append(time_thrust, msg.timestamp/1e6)
        thrust = np.append(thrust, -msg.thrust_body[2])
    for _, msg in (flight_data["/"+name+"/VehicleAcceleration_PubSubTopic"]):
        if msg.timestamp_sample/1e6 not in time_acc:  # No duplicates
            time_acc = np.append(time_acc, msg.timestamp_sample/1e6)
            acceleration = np.append(acceleration, -msg.xyz[2])
    # Sort time (might be locally unordered because of time_sync)
    time_acc, acceleration = zip(*sorted(zip(time_acc, acceleration), key=itemgetter(0)))
    for _, msg in (flight_data["/"+name+"/EscStatus_PubSubTopic"]):
        if msg.esc[0].esc_rpm != 0:
            time_rotors = np.append(time_rotors, msg.timestamp)
            rotors_vel_square_sum = np.append(rotors_vel_square_sum,
                                              msg.esc[0].esc_rpm**2 +
                                              msg.esc[1].esc_rpm**2 +
                                              msg.esc[2].esc_rpm**2 +
                                              msg.esc[3].esc_rpm**2)
    if len(rotors_vel_square_sum) > 2:
        velocity_check = True

    time = np.arange(time_thrust[0], time_thrust[-1], 0.01)

    # Interpolations
    acc_spline = CubicSpline(time_acc, acceleration)
    acc_interpolated = acc_spline(time)

    thrust_spline = CubicSpline(time_thrust, thrust)
    thrust_interpolated = thrust_spline(time)

    if velocity_check:
        rotors_spline = CubicSpline(time_rotors, rotors_vel_square_sum)
        rotors_interpolated = rotors_spline(time)

    fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, figsize=(10, 24))
    ax1.plot(time, thrust_interpolated, 'r')
    ax1.plot(time, acc_interpolated, 'b')

    def onselect(tmin, tmax):
        indmin, indmax = np.searchsorted(time, (tmin, tmax))
        indmax = min(len(time) - 1, indmax)
        x = thrust_interpolated[indmin:indmax]
        y = acc_interpolated[indmin:indmax]
        t = time[indmin: indmax]
        if velocity_check:
            yb = rotors_interpolated[indmin:indmax]
            xb = thrust_interpolated[indmin:indmax]
        ax2.clear()
        ax2.scatter(x, y, s=0.5)
        if velocity_check:
            ax3.clear()
            ax3.scatter(xb, yb, s=0.5)

        x = x[:, np.newaxis]
        a, _, _, _ = np.linalg.lstsq(x, y, rcond=None)
        ax2.plot(x, a*x, 'r-')
        ax4.clear()
        ax4.plot(t, a*x)
        ax4.plot(t, y)
        fig.canvas.draw()
        print("Drone gain = " + str(a) + "* drone_mass")

    span = SpanSelector(ax1, onselect, 'horizontal', useblit=True,
                        rectprops=dict(alpha=0.5, facecolor='red'))
    plt.show()


def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('drone_name', type=str, help='Drone ros namespace')
    args = parser.parse_args()

    root = Tk()
    root.withdraw()
    root.filename = filedialog.askopenfilename()
    flight_data = deserialize_rosbag(root.filename)
    root.destroy()
    thrust_estimation(flight_data, args.drone_name)


if __name__ == 'main':
    main()
