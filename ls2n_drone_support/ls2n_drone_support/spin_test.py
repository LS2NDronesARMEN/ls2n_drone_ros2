# TODO This should be reviewed to use the fake or real joystick and use the frame transforms in drone_bridge
# TODO this version is just for quick test WITHOUT the blades!
import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data, qos_profile_parameters
from std_msgs.msg import Bool
from std_msgs.msg import Float32
from std_srvs.srv import Empty
from ls2n_interfaces.srv import StartControl
from ls2n_interfaces.msg import *
from px4_msgs.msg import VehicleOdometry
import numpy as np
qos_profile_sensor_data.depth = 1


class SpinTest(Node):
    def __init__(self):
        super().__init__('spin_test')
        self.drone_name = "/Drone0"
        self.get_logger().info("Starting Spin Test")
        # Keep alive
        self.keep_alive_publisher = self.create_publisher(Bool, self.drone_name + '/KeepAlive',
                                                          qos_profile_sensor_data)
        self.create_timer(0.1, self.keep_alive_callback)
        # Service clients
        self.spin_motor_client = self.create_client(Empty, self.drone_name + '/SpinMotors')
        self.attitude_control_client = self.create_client(StartControl, self.drone_name + '/StartControl')
        # Publishers
        self.attitude_publisher = self.create_publisher(AttitudeThrustSetPoint,
                                                        self.drone_name + '/AttitudeThrustSetPoint',
                                                        qos_profile_sensor_data)
        # Subscritpions
        # TODO This should not be directly received here but it should go through drone bridge (for frame transforms)
        self.odometry_subscription = self.create_subscription(VehicleOdometry,
                                                              self.drone_name + '/VehicleOdometry_PubSubTopic',
                                                              self.odom_callback,
                                                              qos_profile_sensor_data)
        self.status_subscription = self.create_subscription(DroneStatus,
                                                            self.drone_name + '/Status',
                                                            self.status_callback,
                                                            qos_profile_sensor_data)
        self.spin_vel_subscription = self.create_subscription(Float32,
                                                              self.drone_name + '/SpinVelocity',
                                                              self.spin_vel_callback,
                                                              qos_profile_parameters)
        self.status = DroneStatus.IDLE
        self.spin_vel = 0.3

    def keep_alive_callback(self):
        msg = Bool()
        msg.data = True
        self.keep_alive_publisher.publish(msg)

    def odom_callback(self, msg):
        msg_send = AttitudeThrustSetPoint()
        # TODO put this transformation in the drone bridge
        msg_send.attitude = np.array([msg.q[0], msg.q[1], -msg.q[2], -msg.q[3]]).astype("float64")
        msg_send.thrust = self.spin_vel*10.75*4  # Multiplied by max thrust that is scaled back in drone_bridge
        self.attitude_publisher.publish(msg_send)

    def status_callback(self, msg):
        self.status = msg.status

    def spin_vel_callback(self, msg):
        self.spin_vel = max(0.0, min(msg.data, 1.0))


def main(args=None):
    rclpy.init(args=args)
    spin_test = SpinTest()
    try:
        while not spin_test.spin_motor_client.wait_for_service(timeout_sec=0.5):
            spin_test.get_logger().info('drone services not available, waiting again...')
            rclpy.spin_once(spin_test)
        spin_test.spin_motor_client.call_async(Empty.Request())
        while not (spin_test.status == DroneStatus.ARMED):
            rclpy.spin_once(spin_test)
        spin_test.get_logger().info('Drone ARMED, start spinning at desired setpoint')
        request = StartControl.Request()
        request.control_mode = StartControl.Request.ATTITUDE_THRUST
        spin_test.attitude_control_client.call_async(request)
        rclpy.spin(spin_test)
    except KeyboardInterrupt:
        print('Shutting down drone bridge')
    finally:
        spin_test.destroy_node()
        rclpy.shutdown()


if __name__ == 'main':
    main()
