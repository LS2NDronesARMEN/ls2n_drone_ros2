import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from threading import Thread
from mttkinter import mtTkinter as Tkinter
from guizero import App, PushButton, TextBox, Text
import matplotlib.pyplot as plt
from std_srvs.srv import Empty
from ls2n_interfaces.msg import *
from ls2n_interfaces.srv import *
from rcl_interfaces.msg import Parameter, ParameterType, ParameterValue
from nav_msgs.msg import Odometry
from rcl_interfaces.srv import GetParameters, SetParameters
import time
qos_profile_sensor_data.depth = 1


class PIDTuning(Node):
    def __init__(self, p_curr, d_curr, i_curr, p_set, d_set, i_set):
        super().__init__('position_pid_tuning')

        self.get_logger().info("Starting position pid tuning")
        self.trajectory_publisher = self.create_publisher(
            Trajectory,
            "Trajectory",
            qos_profile_sensor_data
        )
        self.create_subscription(
            Odometry,
            "EKF/odom",
            self.odom_callback,
            qos_profile=qos_profile_sensor_data
        )
        self.create_service(
            Empty,
            "StartExperiment",
            self.start_experiment
        )
        self.start_control_client = self.create_client(
            StartControl,
            "StartControl"
        )
        self.get_param_client = self.create_client(GetParameters,
                                                   'drone_rtps_bridge/get_parameters')
        self.set_param_client = self.create_client(SetParameters,
                                                   'drone_rtps_bridge/set_parameters')
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(3, 1, 1)
        self.ax2 = self.fig.add_subplot(3, 1, 2)
        self.ax3 = self.fig.add_subplot(3, 1, 3)
        self.targets_x = []
        self.currents_x = []
        self.targets_y = []
        self.currents_y = []
        self.targets_z = []
        self.currents_z = []
        self.plot()
        plt.ion()
        plt.show()
        self.experiment_started = False
        self.set_point = Trajectory()
        self.x = CoordinateDerivatives()
        self.x.name = "x"
        self.set_point.coordinates.append(self.x)
        self.y = CoordinateDerivatives()
        self.y.name = "y"
        self.set_point.coordinates.append(self.y)
        self.z = CoordinateDerivatives()
        self.z.name = "z"
        self.z.position = 1.0
        self.set_point.coordinates.append(self.z)
        self.yaw = CoordinateDerivatives()
        self.yaw.name = "yaw"
        self.set_point.coordinates.append(self.yaw)
        # Gui gains set and feedback
        self.gains_names = ["kp", "kd", "ki"]
        self.gains_feedback = [p_curr,
                               d_curr,
                               i_curr]
        self.gains_settings = [p_set,
                               d_set,
                               i_set]
        self.position = 0
        self.create_timer(5, self.update_trajectory)
        self.is_land = False

    def start_experiment(self, request, response):
        request_out = StartControl.Request()
        request_out.control_mode = StartControl.Request.POSITION
        self.start_control_client.call_async(request_out)
        return response

    def receive_parameters(self, future):
        try:
            result = future.result()
        except Exception as e:
            self.get_logger().warn("service call failed %r" % (e,))
        else:
            for param, holder, setter in zip(result.values, self.gains_feedback, self.gains_settings):
                holder.value = param.double_value
                setter.value = param.double_value

    def plot(self):
        self.ax1.clear()
        self.ax1.plot(self.targets_x, 'r', label="target")
        self.ax1.plot(self.currents_x, 'b', label="measure")
        self.ax1.legend()
        self.ax1.set_title("Position x")
        self.ax2.clear()
        self.ax2.plot(self.targets_y, 'r', label="target")
        self.ax2.plot(self.currents_y, 'b', label="measure")
        self.ax2.legend()
        self.ax2.set_title("Position y")
        self.ax3.clear()
        self.ax3.plot(self.targets_z, 'r', label="target")
        self.ax3.plot(self.currents_z, 'b', label="measure")
        self.ax3.legend()
        self.ax3.set_title("Position z")

    def update_gains(self):
        req = GetParameters.Request()
        req.names = [name for name in self.gains_names]
        future_get = self.get_param_client.call_async(req)
        future_get.add_done_callback(self.receive_parameters)

    def set_gains(self):
        param_set_list = []
        for setter, name in zip(self.gains_settings, self.gains_names):
            param_double = Parameter(name=name, value=ParameterValue(
                type=ParameterType.PARAMETER_DOUBLE, double_value=float(setter.value)))
            param_set_list.append(param_double)
        req = SetParameters.Request()
        req.parameters = param_set_list
        self.set_param_client.call_async(req)
        time.sleep(0.1)
        self.update_gains()

    def update_trajectory(self):
        if not self.is_land:
            self.position += 1
            if self.position > 4:
                self.position = 0
            if self.position == 0:
                self.x.position = 0.5
                self.y.position = 0.5
            elif self.position == 1:
                self.x.position = -0.5
                self.y.position = 0.5
            elif self.position == 2:
                self.x.position = -0.5
                self.y.position = -0.5
            elif self.position == 3:
                self.x.position = 0.5
                self.y.position = -0.5
            elif self.position == 4:
                if self.z.position == 1.0:
                    self.z.position = 2.0
                else:
                    self.z.position = 1.0
            self.trajectory_publisher.publish(self.set_point)

    def odom_callback(self, msg):
        self.currents_x.append(msg.pose.pose.position.x)
        self.currents_y.append(msg.pose.pose.position.y)
        self.currents_z.append(msg.pose.pose.position.z)
        self.targets_x.append(self.x.position)
        self.targets_y.append(self.y.position)
        self.targets_z.append(self.z.position)

        if len(self.targets_x) > 200:
            self.targets_x = self.targets_x[1:]
            self.currents_x = self.currents_x[1:]
            self.targets_y = self.targets_y[1:]
            self.currents_y = self.currents_y[1:]
            self.targets_z = self.targets_z[1:]
            self.currents_z = self.currents_z[1:]
        plt.ioff()
        self.plot()
        plt.ion()
        plt.show()

    def land(self):
        self.is_land = True
        self.x.position = 0.0
        self.y.position = 0.0
        self.z.position = 0.05
        self.yaw.position = 0.0
        self.trajectory_publisher.publish(self.set_point)


def main(args=None):
    rclpy.init(args=args)
    gui = App(title="PID Tunning", layout="grid", width=250, height=250)

    p_curr = Text(gui, "0.0", grid=[1, 2])
    d_curr = Text(gui, "0.0", grid=[1, 3])
    i_curr = Text(gui, "0.0", grid=[1, 4])
    p_set = TextBox(gui, "0.0", grid=[2, 2])
    d_set = TextBox(gui, "0.0", grid=[2, 3])
    i_set = TextBox(gui, "0.0", grid=[2, 4])

    node = PIDTuning(p_curr, d_curr, i_curr, p_set, d_set, i_set)

    Text(gui, "Current", grid=[1, 1])
    Text(gui, "Set", grid=[2, 1])
    Text(gui, "Rate P", grid=[0, 2])
    Text(gui, "Rate D", grid=[0, 3])
    Text(gui, "Rate I", grid=[0, 4])
    PushButton(gui, command=node.set_gains, text="Set Values", grid=[1, 7])
    PushButton(gui, command=node.land, text="Land", grid=[1, 9])

    node_thread = Thread(target=rclpy.spin, args=(node,))
    node_thread.daemon = True
    node_thread.start()
    try:
        while not node.get_param_client.wait_for_service(timeout_sec=1.0):
            pass
        node.update_gains()
        gui.display()
    except KeyboardInterrupt:
        pass


if __name__ == 'main':
    main()
