import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data, qos_profile_parameters
from std_srvs.srv import Empty
from std_msgs.msg import Bool, Float32MultiArray
from ls2n_interfaces.msg import *
from ls2n_interfaces.srv import *
from nav_msgs.msg import Odometry
from math import sqrt, copysign
qos_profile_sensor_data.depth = 1


class ThrustTest(Node):
    def __init__(self):
        super().__init__('thrust_test')
        self.get_logger().info("Starting thrust test node")
        self.attitude_thrust_publisher = self.create_publisher(
            AttitudeThrustSetPoint,
            'AttitudeThrustSetPoint',
            qos_profile_sensor_data
        )
        self.keep_alive_publisher= self.create_publisher(
            Bool,
            'KeepAlive',
            qos_profile_sensor_data
        )
        self.create_subscription(
            Odometry,
            "EKF/odom",
            self.odom_callback,
            qos_profile=qos_profile_sensor_data
        )
        self.create_subscription(
            Bool,
            "/CommandCenter/KeepAlive",
            self.keep_alive,
            qos_profile=qos_profile_sensor_data
        )
        self.create_service(
            Empty,
            "/CommandCenter/SpinMotors",
            self.spin_motors_callback
        )
        self.create_service(
            Empty,
            "/CommandCenter/StartExperiment",
            self.start_experiment_callback
        )
        self.spin_motors_client = self.create_client(
            Empty,
            "SpinMotors"
        )
        self.start_control_client = self.create_client(
            StartControl,
            "StartControl"
        )
        self.create_subscription(
            Float32MultiArray,
            "/ThrustTestInputs",
            self.update_inputs,
            qos_profile=qos_profile_parameters
        )
        self.create_timer(0.01, self.publish_thrust)
        self.experiment_started = False
        self.set_point = AttitudeThrustSetPoint()
        self.set_point.attitude = [1.0, 0.0, 0.0, 0.0]
        self.set_point.thrust = 0.0
        self.damping = 0.05
        self.offset = 0.0
        self.odometry = Odometry()
        self.max_thrust = 38.0  # TODO: Get it from the drone parameters
        self.time_start = self.get_clock().now()

    def keep_alive(self, msg):
        self.keep_alive_publisher.publish(msg)

    def update_inputs(self,msg):
        self.offset = msg.data[0]
        self.damping = msg.data[1]/100.0

    def odom_callback(self, msg):
        self.odometry = msg

    def spin_motors_callback(self, request, response):
        self.spin_motors_client.call_async(request)
        return response

    def start_experiment_callback(self, request, response):
        drone_request = StartControl.Request()
        drone_request.control_mode = StartControl.Request.ATTITUDE_THRUST
        self.start_control_client.call_async(drone_request)
        self.experiment_started = True
        self.time_start = self.get_clock().now()
        return response

    def publish_thrust(self):
        if self.experiment_started:
            attitude_x = 0.02*self.odometry.pose.pose.position.y
            attitude_y = -0.02*self.odometry.pose.pose.position.x
            self.set_point.attitude = [
                sqrt(1.0 - attitude_x*attitude_x - attitude_y*attitude_y),
                attitude_x,
                attitude_y,
                0.0
            ]
            z = self.odometry.pose.pose.position.z
            min_t = 0.05
            max_t = 0.9
            self.set_point.thrust = (max_t - (max_t - min_t) * (z - 0.5 - self.offset) / 1.5)
            self.set_point.thrust -= copysign(
                self.damping * self.odometry.twist.twist.linear.z * self.odometry.twist.twist.linear.z,
                self.odometry.twist.twist.linear.z)
            if self.set_point.thrust > max_t:
                self.set_point.thrust = max_t
            if self.set_point.thrust < min_t:
                self.set_point.thrust = min_t
            self.set_point.thrust *= self.max_thrust
            if (self.get_clock().now() - self.time_start).nanoseconds > 60e9:
                self.set_point.thrust = 0.2
            self.attitude_thrust_publisher.publish(self.set_point)


def main(args=None):
    rclpy.init(args=args)
    node = ThrustTest()

    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        print("Shutting down thrust test node")
    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == 'main':
    main()
