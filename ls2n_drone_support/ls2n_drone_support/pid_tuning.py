import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from std_srvs.srv import Empty
from ls2n_interfaces.msg import *
from ls2n_interfaces.srv import *
from threading import Thread
from mttkinter import mtTkinter as Tkinter
from guizero import App, PushButton, TextBox, Text
from enum import IntEnum
import matplotlib.pyplot as plt
from px4_msgs.msg import *
from ls2n_interfaces.srv import DroneGainSet
import transforms3d as tf3d
qos_profile_sensor_data.depth = 1


class TuningMode(IntEnum):
    LAND = 0
    ROLL = 1
    PITCH = 2
    YAW = 3


class PIDTuning(Node):
    def __init__(self, k_curr, p_curr, d_curr, i_curr, ppos_curr, k_set, p_set, d_set, i_set, ppos_set):
        super().__init__('pid_tuning')

        self.get_logger().info("Starting pid tuning")
        self.trajectory_publisher = self.create_publisher(
            Trajectory,
            "Trajectory",
            qos_profile_sensor_data
        )
        self.create_subscription(
            VehicleOdometry,
            "VehicleOdometry_PubSubTopic",
            self.odom_callback,
            qos_profile=qos_profile_sensor_data
        )
        self.create_subscription(
            VehicleAttitudeSetpoint,
            "VehicleAttitudeSetpoint_PubSubTopic",
            self.angles_setpoint_callback,
            qos_profile=qos_profile_sensor_data
        )
        self.create_subscription(
            VehicleAngularVelocity,
            "VehicleAngularVelocity_PubSubTopic",
            self.angular_velocity_callback,
            qos_profile=qos_profile_sensor_data
        )
        self.create_subscription(
            VehicleRatesSetpoint,
            "VehicleRatesSetpoint_PubSubTopic",
            self.rates_setpoint_callback,
            qos_profile=qos_profile_sensor_data
        )
        self.set_gain_client = self.create_client(
            DroneGainSet,
            "DroneGainSet"

        )
        self.create_service(
            Empty,
            "StartExperiment",
            self.start_experiment
        )
        self.start_control_client = self.create_client(
            StartControl,
            "StartControl"
        )
        self.create_timer(0.2, self.futures_check)
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(2, 1, 1)
        self.ax2 = self.fig.add_subplot(2, 1, 2)
        self.targets = []
        self.currents = []
        self.targetsD = []
        self.currentsD = []
        self.roll_target = 0.0
        self.pitch_target = 0.0
        self.yaw_target = 0.0
        self.rollD_target = 0.0
        self.pitchD_target = 0.0
        self.yawD_target = 0.0
        self.ax2.plot(self.targets, 'r', label="target")
        self.ax2.plot(self.currents, 'b', label="measure")
        self.ax2.legend()
        self.ax2.set_title("Angles")
        self.ax1.plot(self.targetsD, 'r', label="target")
        self.ax1.plot(self.currentsD, 'b', label="measure")
        self.ax1.legend()
        self.ax1.set_title("Angle rates")
        plt.ion()
        plt.show()
        self.experiment_started = False
        self.set_point = Trajectory()
        self.x = CoordinateDerivatives()
        self.x.name = "x"
        self.set_point.coordinates.append(self.x)
        self.y = CoordinateDerivatives()
        self.y.name = "y"
        self.set_point.coordinates.append(self.y)
        self.z = CoordinateDerivatives()
        self.z.name = "z"
        self.z.position = 1.5
        self.set_point.coordinates.append(self.z)
        self.yaw = CoordinateDerivatives()
        self.yaw.name = "yaw"
        self.set_point.coordinates.append(self.yaw)
        self.tuning_mode = TuningMode.ROLL
        # Gui gains set and feedback
        self.names = ["RATE_K", "RATE_P", "RATE_D", "RATE_I", "_P"]
        self.gains_feedback = [k_curr,
                               p_curr,
                               d_curr,
                               i_curr,
                               ppos_curr]
        self.gains_settings = [k_set,
                               p_set,
                               d_set,
                               i_set,
                               ppos_set]
        self.futures = []
        self.create_timer(10, self.change_side)

    def start_experiment(self, request, response):
        request_out = StartControl.Request()
        request_out.control_mode = StartControl.Request.POSITION
        self.start_control_client.call_async(request_out)
        return response

    def futures_check(self):
        if len(self.futures) > 0:
            for future in self.futures:
                if future[0].done():
                    future[1].value = round(future[0].result().value, 4)
                    future[2].value = round(future[0].result().value, 4)
                    self.futures.remove(future)

    def switch_roll(self):
        self.x.position = 0.0
        self.y.position = 0.5
        self.yaw.position = 0.0
        self.tuning_mode = TuningMode.ROLL
        self.trajectory_publisher.publish(self.set_point)
        self.update_gains()

    def switch_pitch(self):
        self.x.position = 0.5
        self.y.position = 0.0
        self.yaw.position = 0.0
        self.tuning_mode = TuningMode.PITCH
        self.trajectory_publisher.publish(self.set_point)
        self.update_gains()

    def switch_yaw(self):
        self.x.position = 0.0
        self.y.position = 0.0
        self.yaw.position = 0.5
        self.tuning_mode = TuningMode.YAW
        self.trajectory_publisher.publish(self.set_point)
        self.update_gains()

    def prefix(self):
        if self.tuning_mode == TuningMode.ROLL:
            return "MC_ROLL"
        if self.tuning_mode == TuningMode.PITCH:
            return "MC_PITCH"
        if self.tuning_mode == TuningMode.YAW:
            return "MC_YAW"
        return ""

    def update_gains(self):
        for setter, holder, name in zip(self.gains_settings, self.gains_feedback, self.names):
            req = DroneGainSet.Request()
            req.name = self.prefix()+name
            req.value = 666.0  # Code to request the gain instead of setting it
            future = self.set_gain_client.call_async(req)
            self.futures.append((future, holder, setter))

    def set_gains(self):
        for setter, holder, name in zip(self.gains_settings, self.gains_feedback, self.names):
            req = DroneGainSet.Request()
            req.name = self.prefix()+name
            try:
                req.value = float(setter.value)
            except ValueError:
                req.value = float(holder.value)
            future = self.set_gain_client.call_async(req)
            self.futures.append((future, holder, setter))

    def change_side(self):
        if self.tuning_mode == TuningMode.ROLL:
            self.y.position = -self.y.position
        if self.tuning_mode == TuningMode.PITCH:
            self.x.position = -self.x.position
        if self.tuning_mode == TuningMode.YAW:
            self.yaw.position = -self.yaw.position
        self.trajectory_publisher.publish(self.set_point)

    def odom_callback(self, msg):
        roll, pitch, yaw = tf3d.euler.mat2euler(tf3d.quaternions.quat2mat(msg.q), 'rxyz')
        if self.tuning_mode == TuningMode.ROLL:
            self.targets.append(self.roll_target)
            self.currents.append(roll)
        if self.tuning_mode == TuningMode.PITCH:
            self.targets.append(self.pitch_target)
            self.currents.append(pitch)
        if self.tuning_mode == TuningMode.YAW:
            self.targets.append(self.yaw_target)
            self.currents.append(yaw)
        if len(self.targets) > 200:
            self.targets = self.targets[1:]
            self.currents = self.currents[1:]
        plt.ioff()
        self.ax2.clear()
        self.ax2.plot(self.targets, 'r', label="target")
        self.ax2.plot(self.currents, 'b', label="measure")
        self.ax2.legend()
        self.ax2.set_title("Angles")
        plt.ion()
        plt.show()

    def angles_setpoint_callback(self, msg):
        self.roll_target, self.pitch_target, self.yaw_target \
            = tf3d.euler.mat2euler(tf3d.quaternions.quat2mat(msg.q_d), 'rxyz')

    def angular_velocity_callback(self, msg):
        if self.tuning_mode == TuningMode.ROLL:
            self.targetsD.append(self.rollD_target)
            self.currentsD.append(msg.xyz[0])
        if self.tuning_mode == TuningMode.PITCH:
            self.targetsD.append(self.pitchD_target)
            self.currentsD.append(msg.xyz[1])
        if self.tuning_mode == TuningMode.YAW:
            self.targetsD.append(self.yawD_target)
            self.currentsD.append(msg.xyz[2])
        if len(self.targetsD) > 200:
            self.targetsD = self.targetsD[1:]
            self.currentsD = self.currentsD[1:]
        plt.ioff()
        self.ax1.clear()
        self.ax1.plot(self.targetsD, 'r', label="target")
        self.ax1.plot(self.currentsD, 'b', label="measure")
        self.ax1.legend()
        self.ax1.set_title("Angle rates")
        plt.ion()
        plt.show()

    def rates_setpoint_callback(self, msg):
        self.rollD_target = msg.roll
        self.pitchD_target = msg.pitch
        self.yawD_target = msg.yaw

    def land(self):
        self.x.position = 0.0
        self.y.position = 0.0
        self.z.position = 0.05
        self.yaw.position = 0.0
        self.tuning_mode = TuningMode.LAND
        self.trajectory_publisher.publish(self.set_point)


def main(args=None):
    rclpy.init(args=args)
    gui = App(title="PID Tunning", layout="grid", width=250, height=300)

    k_curr = Text(gui, "0.0", grid=[1, 2])
    p_curr = Text(gui, "0.0", grid=[1, 3])
    d_curr = Text(gui, "0.0", grid=[1, 4])
    i_curr = Text(gui, "0.0", grid=[1, 5])
    ppos_curr = Text(gui, "0.0", grid=[1, 6])
    k_set = TextBox(gui, "0.0", grid=[2, 2])
    p_set = TextBox(gui, "0.0", grid=[2, 3])
    d_set = TextBox(gui, "0.0", grid=[2, 4])
    i_set = TextBox(gui, "0.0", grid=[2, 5])
    ppos_set = TextBox(gui, "0.0", grid=[2, 6])

    node = PIDTuning(k_curr, p_curr, d_curr, i_curr, ppos_curr, k_set, p_set, d_set, i_set, ppos_set)

    PushButton(gui, command=node.switch_roll, text="Roll", grid=[0, 0])
    PushButton(gui, command=node.switch_pitch, text="Pitch", grid=[1, 0])
    PushButton(gui, command=node.switch_yaw, text="Yaw", grid=[2, 0])
    Text(gui, "Current", grid=[1, 1])
    Text(gui, "Set", grid=[2, 1])
    Text(gui, "Rate K", grid=[0, 2])
    Text(gui, "Rate P", grid=[0, 3])
    Text(gui, "Rate D", grid=[0, 4])
    Text(gui, "Rate I", grid=[0, 5])
    Text(gui, "Angle P", grid=[0, 6])
    PushButton(gui, command=node.set_gains, text="Set Values", grid=[1, 7])
    PushButton(gui, command=node.land, text="Land", grid=[1, 9])

    node_thread = Thread(target=rclpy.spin, args=(node,))
    node_thread.daemon = True
    node_thread.start()
    try:
        while not node.set_gain_client.service_is_ready():
            pass
        node.switch_roll()
        gui.display()
    except KeyboardInterrupt:
        pass


if __name__ == 'main':
    main()
