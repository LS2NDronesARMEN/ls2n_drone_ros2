from launch.actions import ExecuteProcess
from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch.conditions import IfCondition, UnlessCondition
from launch_ros.substitutions import FindPackageShare
from ament_index_python.packages import get_package_share_directory
from datetime import datetime
import os
import yaml

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H:%M:%S")
folders_config_file = os.path.join(
    get_package_share_directory('ls2n_drone_command_center'),
    'config',
    'folders.yaml'
)
with open(folders_config_file) as file:
    folders_config = yaml.load(file, Loader=yaml.FullLoader)
log_folder = folders_config['ls2n_drone_command_center']['launch_parameters']['logs_folder']


def generate_launch_description():
    return LaunchDescription([
        DeclareLaunchArgument("drone_namespace", default_value="Drone1",
                              description="Drone namespace"),
        DeclareLaunchArgument("esc_feedback", default_value="False",
                              description="Set true to record esc feedback (not available in simulation)"),
        Node(package='ls2n_drone_support', executable='thrust_test',
             output='screen', namespace=LaunchConfiguration("drone_namespace")),
        Node(package='slider_publisher', executable='slider_publisher',
             output='screen', arguments=[[FindPackageShare('ls2n_drone_support'),
                                         '/config/sliders_thrust_test.yaml']])
    ])
