[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

LS2N_drone_ROS2
=====================
LS2N_drone_ROS2 is a repository containing ROS2 packages aimed to control or perform a simulation of the drones
available in the LS2N laboratory using ROS2. For simulation, we focus on SITL and HITL approaches to facilitate the
transition toward real life experiments. For more details about each package, please have a look in the subfolders
README files.

Prerequisites
--------------------------
This environment
requires [ros2 galactic](https://docs.ros.org/en/galactic/Installation/Ubuntu-Install-Debians.html#install-ros-2-packages)
and gazebo to work. This file assumes that the reader is accustomed to ROS2.

It seems that there are issues when several versions of ROS2 are installed (seen with foxy/galactic on Ubuntu 20.04).
Please check that there are nos ROS version issues when you source your workspace. Note that this might be something to
fix in a package (I suspect px4_ros_com).

The launch files may use *gnome-terminal* to split the outputs in several terminals (therefore, *gnome-terminal* should
be installed, or some launch files should be modified to be adapted to your specific terminal).

1. Clone this repo in the src folder of you ROS2 workspace. You should clone recursively as it contains submodules. If
   you already cloned it you can still get the submodules using
   ```console
   git submodule init
   git submodule update
    ```
2. Install the following packages: ros-galactic-gazebo-ros ros-galactic-gazebo-ros-pkgs libgstreamer1.0-dev
   openjdk-8-jdk python3-colcon-common-extensions python3-pip ros-galactic-tf2-ros

3. Install with pip3: pyros-genmsg transforms3d guizero scipy qtm pymavlink mttkinter jinja2 inputs toml

4. Install [Fast-RTPS-Gen](https://docs.px4.io/master/en/dev_setup/fast-dds-installation.html#fast-rtps-gen).

5. Build the packages using colcon and source the setup script of your workspace. First build does not generate the
   trajectories, so you might want to do it twice. The eigen library might be not found properly, in this case you
   should creat
   a [symlink](https://summeridiot.wordpress.com/2013/01/20/use-symlink-to-link-the-library-to-usrlocalinclude/).

6. Clone **recursively** the [PX4-Autopilot](https://gitlab.com/LS2NDronesARMEN/px4-autopilot) **outside of your ROS2
   workspace**. The repository is a fork from the original project [PX4-Autopilot](https://github.com/PX4/PX4-Autopilot)
   including the LS2N drones models and configurations. You must use the ls2n-drone-simulation branch to work with
   ls2n_drone_ros2 packages. Init and update submodules
   ```console
   git submodule init
   git submodule update
   ```

Both repositories *px4-autopilot* and *ls2n_drone_ros2* compatibility is ensured on the last commit only. Always pull
those two repos at the same time and update your submodules.

Simulation
---------------------------
To start a simulation, please have a look in the ls2n_drone_simulation package and follow the README instructions.

Clean build
-----------------
Some updates (especially with the RTPS protocol) require a clean build.

- On PX4 firmware (example for simulation)
   ```console
   make clean && DONT_RUN=1 make px4_sitl_rtps gazebo
    ```
- On ROS2 packages (in your ros2 workspace)
     ```console
   ./src/ls2n_drone_ros2/px4_ros_com/scripts/clean_all.bash 
   sudo rm -r log build install
   colcon build
    ```
  
