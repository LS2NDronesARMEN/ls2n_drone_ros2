from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration
from launch.launch_description_sources import PythonLaunchDescriptionSource
from ament_index_python.packages import get_package_share_directory
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        DeclareLaunchArgument("drone_namespace", default_value="Drone1",
                              description="Drone namespace"),
        IncludeLaunchDescription(PythonLaunchDescriptionSource([get_package_share_directory('ls2n_tools'),
                                                                    '/mocap.launch.py'])),
        Node(package='ls2n_drone_command_center', executable='joystick',
             output='screen', namespace='CommandCenter'),
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                get_package_share_directory('ls2n_drone_support'), '/identification.launch.py'
            ]),
            launch_arguments={
                "drone_namespace": LaunchConfiguration("drone_namespace"),
                "esc_feedback": "True"
            }.items()
        )
    ])
