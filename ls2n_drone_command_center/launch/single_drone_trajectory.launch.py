from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch.conditions import IfCondition, UnlessCondition
from ament_index_python.packages import get_package_share_directory
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        DeclareLaunchArgument("sitl", default_value="False",
                              description="Set true for SITL simulation"),
        DeclareLaunchArgument("drone_namespace", default_value="Drone1",
                              description="Drone namespace"),
        DeclareLaunchArgument("trajectory", default_value="single_drone_hovering",
                              description="Trajectory to play"),

        IncludeLaunchDescription(PythonLaunchDescriptionSource(
            [get_package_share_directory('ls2n_tools'), '/mocap.launch.py']),
            condition=UnlessCondition(LaunchConfiguration("sitl"))
        ),
        Node(package='ls2n_drone_command_center', executable='joystick',
             output='screen', namespace=LaunchConfiguration('drone_namespace'),
             condition=UnlessCondition(LaunchConfiguration("sitl"))),

        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([get_package_share_directory('ls2n_drone_simulation'),
                                          '/drones_sitl.launch.py']),
            launch_arguments={'param_file': 'single_drone_params.yaml',
                              'gz_world': 'empty'}.items(),
            condition=IfCondition(LaunchConfiguration("sitl"))
        ),
        Node(package='ls2n_drone_command_center', executable='fake_joystick',
             output='screen', namespace=LaunchConfiguration('drone_namespace'),
             condition=IfCondition(LaunchConfiguration("sitl"))),

        Node(package='ls2n_drone_command_center', executable='trajectory_publisher',
             output='screen', namespace=LaunchConfiguration('drone_namespace'),
             parameters=[{"trajectory": LaunchConfiguration('trajectory')}])
    ])
