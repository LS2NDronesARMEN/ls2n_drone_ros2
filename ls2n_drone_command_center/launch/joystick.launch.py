import os
import launch
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():

    joy_params = os.path.join(
        os.path.join(get_package_share_directory('ls2n_drone_command_center'), 'config'),
        'joy-params.yaml')
    
    joy_node = Node(
        package='joy',
        executable='joy_node',
        output='screen',
        parameters=[joy_params],
        remappings=[("joy", "CommandCenter/joystick")])

    return LaunchDescription([
        joy_node,
        launch.actions.RegisterEventHandler(
            event_handler=launch.event_handlers.OnProcessExit(
                target_action=joy_node,
                on_exit=[launch.actions.EmitEvent(event=launch.events.Shutdown())],
        )),

        Node(
            package='ls2n_drone_command_center',
            executable='joystick_v2', 
            output='screen', 
            namespace='CommandCenter'
        ),
    ])
