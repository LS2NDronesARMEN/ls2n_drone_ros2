[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

ls2n_drone_command_center
=====================
A package containing the tools and launch files required to remotely control one or several drones at the LS2N
laboratory.

One should use the launch files to pop all the required node for a given experiment.