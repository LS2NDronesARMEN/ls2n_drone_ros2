import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from std_srvs.srv import Empty
from ls2n_interfaces.msg import *
from ls2n_interfaces.srv import *
from rcl_interfaces.msg import ParameterType
from rcl_interfaces.msg import ParameterDescriptor
from ament_index_python.packages import get_package_share_directory
import os
import pickle
qos_profile_sensor_data.depth = 1


class TrajectoryPublisher(Node):
    def __init__(self):
        super().__init__('trajectory_publisher')
        trajname_descriptor = ParameterDescriptor(type=ParameterType.PARAMETER_STRING,
                                                  description='Name of the trajectory')
        self.declare_parameter("trajectory", None, trajname_descriptor)
        trajectory_name = self.get_parameter("trajectory").value
        if trajectory_name is None:
            exit("Please provide a trajectory name")
        filename = os.path.join(get_package_share_directory('ls2n_tools'),
                                'trajectories', trajectory_name + ".traj.pickle")
        try:
            file = open(filename, 'rb')
        except FileNotFoundError:
            exit("This trajectory does not exist.")
        self.trajectory = pickle.load(file)

        self.get_logger().info("Starting trajectory publisher")
        self.publisher = self.create_publisher(
            Trajectory,
            "Trajectory",
            qos_profile_sensor_data
        )
        self.create_service(
            Empty,
            "StartExperiment",
            self.start_experiment
        )
        self.create_service(
            Empty,
            "StopExperiment",
            self.stop_experiment
        )
        self.start_control_client = self.create_client(
            StartControl,
            "StartControl"
        )
        self.land_disarm_client = self.create_client(
            Empty,
            "LandDisarm"
        )
        self.init_timer = self.create_timer(0.5, self.init_publish)
        self.publish_timer = None
        self.time_start = self.get_clock().now()
        self.index = 0
        self.index_max = len(self.trajectory["time"]) - 1
        self.experiment_started = False

    def init_publish(self):
        self.publish_trajectory()

    def start_experiment(self, request, response):
        if not self.experiment_started:
            self.get_logger().info("Starting experiment trajectory")
            request_out = StartControl.Request()
            request_out.control_mode = StartControl.Request.POSITION
            self.start_control_client.call_async(request_out)
            self.destroy_timer(self.init_timer)
            self.time_start = self.get_clock().now()
            self.publish_timer = self.create_timer(0.001, self.check_and_publish)
            self.index = 0
            self.experiment_started = True
        else:
            self.get_logger().info("Experiment already started. Restart the node to reset.")
        return response

    def stop_experiment(self, request, response):
        if self.experiment_started:
            self.get_logger().info("Stopping experiment trajectory")
            self.land_disarm_client.call_async(Empty.Request())
            # Reset everything
            self.experiment_started = False
            self.destroy_timer(self.publish_timer)
            self.index = 0
            self.init_timer = self.create_timer(0.5, self.init_publish)
        return response

    def check_and_publish(self):
        time_elapsed = (self.get_clock().now() - self.time_start).nanoseconds/1e9
        if self.index < self.index_max:
            if self.trajectory["time"][self.index + 1] < time_elapsed:
                self.index += 1
                self.publish_trajectory()

    def publish_trajectory(self):
        msg = Trajectory()
        msg.trajectory_time = self.trajectory["time"][self.index]
        for coordinate in self.trajectory.keys():
            if coordinate != "time" and coordinate[-1] != "D":
                var_msg = CoordinateDerivatives()
                var_msg.name = coordinate
                var_msg.position = self.trajectory[coordinate][self.index]
                if coordinate+"D" in self.trajectory:
                    var_msg.velocity = self.trajectory[coordinate+"D"][self.index]
                if coordinate+"DD" in self.trajectory:
                    var_msg.acceleration = self.trajectory[coordinate+"DD"][self.index]
                if coordinate+"DDD" in self.trajectory:
                    var_msg.jerk = self.trajectory[coordinate+"DDD"][self.index]
                msg.coordinates.append(var_msg)
        self.publisher.publish(msg)


def main(args=None):
    rclpy.init(args=args)
    trajectory_publisher = TrajectoryPublisher()
    try:
        rclpy.spin(trajectory_publisher)
    except KeyboardInterrupt:
        print('Shutting down trajectory publisher')
    finally:
        trajectory_publisher.destroy_node()
        rclpy.shutdown()


if __name__ == 'main':
    main()
