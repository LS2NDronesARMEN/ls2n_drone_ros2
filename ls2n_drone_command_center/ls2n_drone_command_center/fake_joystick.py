import rclpy
from threading import Thread
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from std_msgs.msg import Bool
from std_srvs.srv import Empty
from guizero import App, PushButton, CheckBox
qos_profile_sensor_data.depth = 1


class FakeJoystick(Node):
    def __init__(self, keep_alive):
        super().__init__('fake_joystick')
        self.get_logger().info("Starting fake joystick")
        # Keep alive
        self.keep_alive_publisher = self.create_publisher(Bool, 'KeepAlive',
                                                          qos_profile_sensor_data)
        self.create_timer(0.01, self.keep_alive_callback)
        # Service clients
        self.spin_motor_client = self.create_client(Empty, 'SpinMotors')
        self.start_experiment_client = self.create_client(Empty, 'StartExperiment')
        self.stop_experiment_client = self.create_client(Empty, 'StopExperiment')
        self.keep_alive = keep_alive

    def keep_alive_callback(self):
        if self.keep_alive is not None:
            if self.keep_alive.value == 1:
                msg = Bool()
                msg.data = True
                self.keep_alive_publisher.publish(msg)

    def spin_motor(self):
        self.spin_motor_client.call_async(Empty.Request())

    def stop_experiment(self):
        self.stop_experiment_client.call_async(Empty.Request())

    def star_experiment(self):
        self.start_experiment_client.call_async(Empty.Request())


def main(args=None):
    rclpy.init(args=args)
    gui = App(title="Fake Joystick", layout="grid", width=500, height=250)
    keep_alive_box = CheckBox(gui, text="RT - Keep alive", grid=[5, 0])
    keep_alive_box.toggle()
    fake_joystick = FakeJoystick(keep_alive_box)
    PushButton(gui, command=fake_joystick.spin_motor, text="A - Spin motor", grid=[5, 3])
    PushButton(gui, command=fake_joystick.star_experiment, text="B - Start Experiment", grid=[6, 2])
    PushButton(gui, command=fake_joystick.stop_experiment, text="X - Stop Experiment", grid=[4, 2])
    node_thread = Thread(target=rclpy.spin, args=(fake_joystick,))
    node_thread.daemon = True
    node_thread.start()
    try:
        gui.display()
    except KeyboardInterrupt:
        pass
    print("Shutting down fake joystick")


if __name__ == 'main':
    main()
