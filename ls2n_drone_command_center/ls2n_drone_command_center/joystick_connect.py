import rclpy
from rclpy.node import Node
from rclpy.qos import qos_profile_sensor_data
from std_msgs.msg import Bool
from std_srvs.srv import Empty
from inputs import get_gamepad, UnpluggedError
from threading import Thread
qos_profile_sensor_data.depth = 1


class JoystickConnect(Node):
    def __init__(self):
        super().__init__('joystick')
        self.get_logger().info("Starting joystick connection")
        # Keep alive
        self.keep_alive_publisher = self.create_publisher(Bool, 'KeepAlive',
                                                          qos_profile_sensor_data)
        self.create_timer(0.01, self.keep_alive_callback)
        # Service clients
        self.spin_motor_client = self.create_client(Empty, 'SpinMotors')
        self.start_experiment_client = self.create_client(Empty, 'StartExperiment')
        self.keep_alive = 0
        self.active = True
        self.th = Thread(target=self.update_joystick)
        self.th.daemon = True
        self.th.start()

    def update_joystick(self):
        while self.active:
            events = []
            try:
                events = get_gamepad()
            except UnpluggedError:
                self.get_logger().error("No joystick plugged")
                self.active = False

            for event in events:
                if event.code == 'BTN_C':  # A in our joysticks
                    if event.state == 1:
                        self.spin_motor()
                elif event.code == 'BTN_EAST':  # B
                    if event.state == 1:
                        self.star_experiment()
                elif event.code == 'BTN_TR':  # RT
                    self.keep_alive = event.state

    def keep_alive_callback(self):
        if self.keep_alive == 1:
            msg = Bool()
            msg.data = True
            self.keep_alive_publisher.publish(msg)

    def spin_motor(self):
        self.spin_motor_client.call_async(Empty.Request())

    def star_experiment(self):
        self.start_experiment_client.call_async(Empty.Request())


def main(args=None):
    rclpy.init(args=args)
    joystick_connect = JoystickConnect()
    try:
        rclpy.spin(joystick_connect)
    except KeyboardInterrupt:
        print("Shutting down joystick connection")
    finally:
        joystick_connect.destroy_node()
        rclpy.shutdown()


if __name__ == 'main':
    main()
