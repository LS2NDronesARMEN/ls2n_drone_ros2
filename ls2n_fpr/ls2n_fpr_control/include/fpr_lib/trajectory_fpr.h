/** @file trajectoryfpr.h
 *  @brief Class for trajectory generation of FPR
 *
 *  @author Shiyu-LIU
 *  @version: v2.0
 *  @date: 11 March 2021
 */
#pragma once

#include "trajectory_base.h"
#include "common_fpr.h"

namespace FPR {

/** 
 *  \brief Derived class from TrajectoryBase for trajectory generation of FPR, 
 * including platform's COM position and its orientation represented by unit quaternion, as well as leg angles.
 * The position defined is relative to the initial position, while orientation and leg angles are absolute values.
 */
class TrajectoryFPR : public TrajectoryBase
{
public:
    // index 0: time; index 1-3: position; 4-7: quaternion for orientation; 8-10: leg angles
    static const int data_size = 11; 
    static const int state_size = 10;
    static const int deriv_size = 9;

    typedef Eigen::Matrix<double, state_size, 1> State_Type; // FPR pose-type vector
    typedef Eigen::Matrix<double, deriv_size, 1> Deriv_Type; // FPR velocity and acceleration-type vector

    TrajectoryFPR(TrajectoryType _type);
    ~TrajectoryFPR(){};

    bool interpolateTraj(std::string& msgerr); 

    void getTrajNow(Eigen::VectorXd& _pos, Eigen::VectorXd& _vel, Eigen::VectorXd& _acc, const double _tNow);

    void setInitPose(Eigen::Vector3d _init_pos, Eigen::Quaterniond _init_orient, VectorNdFPR _init_ang);

protected:

    Eigen::Vector3d init_position; // initial position of the platform
    Eigen::Quaterniond init_orientation; // initial orientation of the platform
    VectorNdFPR init_angle; // initial angles of legs
};

}