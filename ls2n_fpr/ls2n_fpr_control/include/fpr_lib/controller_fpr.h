/** @file controllerFPR.h
 *  @brief Class for controlling the Flying Parallel Robot (FPR)
 *
 *  @author Shiyu-LIU
 *  @version: v2.0
 *  @date: 11 March 2021
 */
#pragma once

#include <eigen3/Eigen/Dense>

#include "common_fpr.h"
#include "state_fpr.h"
#include "model_fpr.h"

namespace FPR {

class ControllerFPR
{

public:
    typedef Eigen::Matrix<double, DIM_DOF+1, 1> PoseType; 
    typedef Eigen::Matrix<double, DIM_DOF, 1> DofType;
    typedef Eigen::Matrix<double, DIM_ACT, 1> ActType;

    enum ControllerType
    {
        PD_CTC,
        PID_CTC,
        IMPEDANCE
    };

    struct ControlErrorState
    {
        DofType pose_error;
        DofType accum_error; // accumulated pose error
        DofType velocity_error; // difference between desired and acutal velocity
    };

    struct DroneTarget
    {  
        double thrust;
        Eigen::Quaterniond attitude;
    };

    // Constructor
    ControllerFPR(ModelFPR& _mdl, ControllerType ctrlType);
    // Destructor
    ~ControllerFPR(){};
    // Spin the controller once
    bool spinController(std::string& msgerr);
    // Update the desired trajectory
    void updateTrajectory(const PoseType& _des_pose, const DofType& _des_vel, const DofType& _des_acc);
    // Update the current robot pose
    void updateCurrentPose(const PoseFPR& _pose);
    // Update the current velocity
    void updateCurrentVelocity(const DofType& _vel);
    // Get drone thrust/attitude commands
    void getDroneTargets(std::vector<DroneTarget>& _target);
    // Get the computed actuation force 
    ActType getActuationForce() { return actuation_force; };
    // Get the controller errors
    ControlErrorState getControlError() { return control_error; };

protected:

    // Kinematic and dynamic model of FPR
    ModelFPR model;
    // Current FPR pose
    PoseFPR pose; 
    // Desired FPR pose
    PoseFPR des_pose; 
    // Current FPR velocity
    DofType velocity;
    // Desired FPR velocity
    DofType des_velocity;
    // Desired FPR acceleration
    DofType des_acceleration;

    // Actuation force that drives the platform and legs (including force and moment exerted on the platform and moments of legs)
    ActType actuation_force;
    // Drone desired targets (desired thrust and attitude)
    DroneTarget drone_target[NUM_LEG];

    // Controller type
    ControllerType controller_type;
    // Control error
    ControlErrorState control_error;

    // Control gain structure
    struct ControlGains
    {
        /* PID control gains */
        Eigen::Matrix<double, DIM_DOF, DIM_DOF> PID_Kp;
        Eigen::Matrix<double, DIM_DOF, DIM_DOF> PID_Kd;
        Eigen::Matrix<double, DIM_DOF, DIM_DOF> PID_Ki;
        /* Impedance control gains */
        Eigen::Matrix<double, DIM_DOF, DIM_DOF> Mv;
        Eigen::Matrix<double, DIM_DOF, DIM_DOF> Dv;
        Eigen::Matrix<double, DIM_DOF, DIM_DOF> Kv;
    } control_gains;

    // External wrench structure
    struct ExternalWrench
    {
        DofType measured; // measured
        DofType desired; // desired
        DofType estimated; // estimated
        double t_mea = -1; // measurement timestamp
        double t_des = -1; // desired value timestamp
        double t_est = -1; // estimation timestamp      
    } external_wrench;

    bool traj_updated;
    bool pose_updated;
    bool vel_updated;

    // Compute the drone targets
    void computeDroneTarget();
    // Motion control law (PID + feedback linearization)
    void doMotionControl();
    // Impedance control law
    void doImpedanceControl();
    //void selectControllerType();
    
    DofType computeControlError();
    DofType computePDControlLaw();
    DofType computePIDControlLaw();
};

}