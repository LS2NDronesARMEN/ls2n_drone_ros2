/** @file modelfpr.h
 *  @brief Class for kinematic and dynamic modeling of FPR 
 *
 *  @author Shiyu-LIU
 *  @version: v2.0
 *  @date: 11 March 2021
 */
#pragma once

#include <eigen3/Eigen/Dense>
#include "common_fpr.h"
#include "param_fpr.h"

namespace FPR{

class ModelFPR
// Class for kinematic and dynamic model of FPR (passive architecture), 
// including a Jacobian matrix, an inertia matrix and a vector of Coriolis, centrifugal and gravitational effects.
{
public:

    Eigen::Matrix<double, DIM_ACT, DIM_DOF> JacobianMat; // jacobian matrix
    Eigen::Matrix<double, DIM_DOF, DIM_DOF> InertiaMat; // inertia matrix
    Eigen::Matrix<double, DIM_DOF, 1> CoriolisVec; // coriolis vector

    Eigen::Matrix3d Rp; // rotation matrix from F0 to Fp
    Eigen::Matrix3d Rleg[NUM_LEG]; // rotation matrix from Fp to Fli

    ParamFPR param;

    ModelFPR(ParamFPR& _pm) : param(_pm)
    {
        JacobianMat.setZero();
        InertiaMat.setZero();
        CoriolisVec.setZero();
        Rp.setZero();
    }

    void computeDynamicModel(const Eigen::Matrix<double, DIM_DOF+1, 1>& current_pos, const Eigen::Matrix<double, DIM_DOF, 1>& current_vel);

    Eigen::Matrix<double, DIM_ACT, DIM_DOF> computeKinematicModel(const Eigen::Matrix<double, DIM_DOF+1, 1>& current_pos);

    void resetParam(ParamFPR _pm)
    {
        param = _pm;
        param.computeParam();
    }

};

}