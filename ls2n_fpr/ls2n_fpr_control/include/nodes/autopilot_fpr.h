#ifndef AUTOPILOT_FPR
#define AUTOPILOT_FPR

#include "rclcpp/rclcpp.hpp"
#include "tf2_eigen/tf2_eigen.h"
#include "tf2_ros/transform_broadcaster.h"
#include "controller_fpr.h"
#include "param_fpr.h"
#include "trajectory_fpr.h"
#include "state_fpr.h"

#include "std_msgs/msg/bool.hpp"
#include "std_srvs/srv/empty.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "geometry_msgs/msg/transform_stamped.hpp"
#include "ls2n_interfaces/msg/attitude_thrust_set_point.hpp"
#include "ls2n_interfaces/msg/drone_status.hpp"
#include "ls2n_interfaces/srv/start_control.hpp"
#include "ls2n_fpr_interfaces/msg/fpr_trajectory.hpp"

using TransformStamped = geometry_msgs::msg::TransformStamped; 

class RosFprAutopilot : public rclcpp::Node
{
public:
    RosFprAutopilot();

private:
    bool loadParams();
    bool trajectoryInit();
    void controlLoop();
    void transferKeepAlive(std_msgs::msg::Bool::SharedPtr msg);
    void spinMotors(const std::shared_ptr<std_srvs::srv::Empty::Request>,
                    std::shared_ptr<std_srvs::srv::Empty::Response>);
    void startExperiment(const std::shared_ptr<std_srvs::srv::Empty::Request>,
                         std::shared_ptr<std_srvs::srv::Empty::Response>);
    void startFlying();
    void updateDronePose(nav_msgs::msg::Odometry::SharedPtr msg, const int& index);
    void updateDroneStatus(ls2n_interfaces::msg::DroneStatus::SharedPtr msg, const int& index);
    void updatePlatformPose(nav_msgs::msg::Odometry::SharedPtr msg);
    void updateTrajectory(ls2n_fpr_interfaces::msg::FprTrajectory::SharedPtr msg);
    void checkDroneStatus();
    void broadcastFrames();

    rclcpp::TimerBase::SharedPtr timer;

    FPR::ParamFPR paramFpr;
    FPR::TrajFPR trajFpr;
    FPR::ControllerFPR::ControllerType controllerType;
    FPR::ControllerFPR* controllerFpr;
    FPR::ModelFPR* modelFpr;
    FPR::TrajectoryFPR* trajectoryFpr;
    FPR::PoseFPR* poseFpr;
    FPR::PoseFPR* initPoseFpr;
    FPR::VelocityFPR* velocityFpr;
    FPR::DroneState poseDrone[NUM_LEG];

    int8_t status;
    std::array<int8_t, NUM_LEG> droneStatus;

    int droneIndexes[NUM_LEG];

    // TransformStamped msgs for all frames
    struct FramesFPR
    {
        TransformStamped platform_frame;
        TransformStamped leg_frames[NUM_LEG];
        TransformStamped drone_frames[NUM_LEG];
        static void set_frame(TransformStamped& frame, const Eigen::Vector3d& p, const Eigen::Quaterniond& q, std::string frame_name)
        {
            frame.header.frame_id = "world";
            frame.child_frame_id = frame_name;
            frame.transform.translation = tf2::toMsg2(p);
            frame.transform.rotation = tf2::toMsg(q);
        };
    } framesFpr;

    // Bool value if broadcast frames to tf2
    bool broadcast_frames;
    // TransformBroadcaster for tf2 frames
    std::shared_ptr<tf2_ros::TransformBroadcaster> tfBroadcaster;

    // Commuunication with trajectory publishing center
    rclcpp::Subscription<ls2n_fpr_interfaces::msg::FprTrajectory>::SharedPtr trajSubscription; 
    
    // Communication with joystick
    rclcpp::Subscription<std_msgs::msg::Bool>::SharedPtr keepAliveSubscription;
    rclcpp::Service<std_srvs::srv::Empty>::SharedPtr spinMotorsService;
    rclcpp::Service<std_srvs::srv::Empty>::SharedPtr startExperimentService;

    // Communication with drone bridges
    rclcpp::Subscription<ls2n_interfaces::msg::DroneStatus>::SharedPtr droneStatusSubscription[NUM_LEG];
    rclcpp::Publisher<std_msgs::msg::Bool>::SharedPtr keepAliveDronePublisher[NUM_LEG];
    rclcpp::Client<std_srvs::srv::Empty>::SharedPtr spinMotorsClient[NUM_LEG];
    rclcpp::Client<ls2n_interfaces::srv::StartControl>::SharedPtr startControlClient[NUM_LEG];
    rclcpp::Publisher<ls2n_interfaces::msg::AttitudeThrustSetPoint>::SharedPtr attitudeThrustPublishDrone[NUM_LEG];

    // Communication with MOCAP - TODO only for Gazebo simulation now, should be updated to work with MOCAP or Fake MOCAP
    rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr odomPlatformSubscription;
    rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr odomDroneSubscription[NUM_LEG];
};

#endif