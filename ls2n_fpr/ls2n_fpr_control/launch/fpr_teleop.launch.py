import os
import launch
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory

def generate_launch_description():

    return LaunchDescription([
        launch.actions.IncludeLaunchDescription(
            launch.launch_description_sources.PythonLaunchDescriptionSource(
                get_package_share_directory('ls2n_drone_command_center') + '/joystick.launch.py')
        ),

        Node(package='ls2n_fpr_control', 
            executable='fpr_teleop_node.py', 
            output='screen', 
            namespace='CommandCenter'
        ),

        Node(package='ls2n_fpr_control', 
            executable='trajectory_publisher_node.py', 
            output='screen', 
            parameters=[
                {'trajectory_mode':'manual'}
            ],
            namespace='CommandCenter'
        ),
    ])