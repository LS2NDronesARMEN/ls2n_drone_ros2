from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
import os


config = os.path.join(
    get_package_share_directory('ls2n_fpr_control'),
    'config',
    'param_fpr.yaml'
)


def generate_launch_description():

    return LaunchDescription([
        Node(package='ls2n_fpr_control', executable='trajectory_publisher_node.py', output='screen', 
             parameters=[
                {'trajectory_mode':'autopilot'},
                {'trajectory_file':'fpr_hovering'}],
             namespace='CommandCenter'
        ),
        
        Node(package='ls2n_fpr_control', 
            executable='autopilot_fpr', 
            output='screen', 
            parameters=[config,
                {"enable_rviz": True},
                {"drone_param.drone0.num": 1},
                {"drone_param.drone1.num": 2},
                {"drone_param.drone2.num": 3}],
            namespace='CommandCenter'),
    ])
