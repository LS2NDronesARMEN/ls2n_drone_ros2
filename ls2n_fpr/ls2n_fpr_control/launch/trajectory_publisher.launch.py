from launch import LaunchDescription
from launch_ros.actions import Node


def generate_launch_description():

    return LaunchDescription([
        Node(package='ls2n_fpr_control', executable='trajectory_publisher_node.py', output='screen', 
             parameters=[
                {'trajectory_mode':'autopilot'},
                {'trajectory_file':'fpr_hovering'}
             ],
             namespace='CommandCenter'
            ),
        Node(package='ls2n_drone_command_center', executable='fake_joystick',
             output='screen', namespace='CommandCenter')
    ])