#include "autopilot_fpr.h"
#include <utility>
#include <ament_index_cpp/get_package_share_directory.hpp>
#include <ament_index_cpp/get_package_prefix.hpp>

using namespace std::chrono_literals;
using DroneStatus = ls2n_interfaces::msg::DroneStatus;
using ControllerType = FPR::ControllerFPR::ControllerType;

RosFprAutopilot::RosFprAutopilot():Node("autopilot_fpr",
                                        rclcpp::NodeOptions()
                                                .automatically_declare_parameters_from_overrides(true)),
                                   status(DroneStatus::IDLE) {
    if (!loadParams())
    {
        RCLCPP_ERROR(get_logger(), "Parameter file or some parameters in the file are missing");
        exit(1);
    }
    paramFpr.computeParam();
    // Strengthen the qos sensor profile (only last kept, queue of 1)
    auto qos_sensor_fpr = rclcpp::SensorDataQoS();
    qos_sensor_fpr.keep_last(1);

    // Initialize the model, he controller and the trajectory
    modelFpr = new FPR::ModelFPR(paramFpr);
    poseFpr = new FPR::PoseFPR(paramFpr);
    initPoseFpr = new FPR::PoseFPR(paramFpr);
    velocityFpr = new FPR::VelocityFPR(*modelFpr);
    controllerFpr = new FPR::ControllerFPR(*modelFpr, controllerType);
    trajectoryFpr = new FPR::TrajectoryFPR(TrajectoryType::Poly7order);
    /*if (!trajectoryInit())
    {
        RCLCPP_ERROR(get_logger(), "Error in trajectory initialization");
        exit(1);
    }*/

    // Create trajectory subscription
    trajSubscription = create_subscription<ls2n_fpr_interfaces::msg::FprTrajectory>("/CommandCenter/fpr_trajectory",
                                                                                    qos_sensor_fpr,
                                                                                    [this](ls2n_fpr_interfaces::msg::FprTrajectory::SharedPtr traj){
                                                                                        updateTrajectory(traj);}
                                                                                    );

    // Create one keep alive subscription and publishers
    keepAliveSubscription = create_subscription<std_msgs::msg::Bool>("KeepAlive",
                                                                     qos_sensor_fpr,
                                                                     [this](std_msgs::msg::Bool::SharedPtr PH1) {
                                                                         transferKeepAlive(PH1); } );
    for (int i=0; i<NUM_LEG; i++)
        keepAliveDronePublisher[i] = create_publisher<std_msgs::msg::Bool>("/Drone"+std::to_string(droneIndexes[i])+"/KeepAlive", qos_sensor_fpr);

    // Creat subscriptions for drones status
    for (int i=0; i<NUM_LEG; i++)
        droneStatusSubscription[i] = create_subscription<ls2n_interfaces::msg::DroneStatus>("/Drone"+std::to_string(droneIndexes[i])+"/Status",
                                                                                            qos_sensor_fpr,
                                                                                            [this, i](ls2n_interfaces::msg::DroneStatus::SharedPtr PH1){
                                                                                                updateDroneStatus(PH1, i);}
                                                                                            );

    // Create one service to spin Motors and clients
    spinMotorsService = create_service<std_srvs::srv::Empty>("SpinMotors",
                                                            [this](std::shared_ptr<std_srvs::srv::Empty::Request> r1,
                                                                    std::shared_ptr<std_srvs::srv::Empty::Response> r2)
                                                            { spinMotors(r1, r2); } );
    for (int i=0; i<NUM_LEG; i++)
        spinMotorsClient[i] = create_client<std_srvs::srv::Empty>("/Drone"+std::to_string(droneIndexes[i])+"/SpinMotors");

    // Create one service to start experiment
    startExperimentService = create_service<std_srvs::srv::Empty>("StartExperiment",
                                                             [this](std::shared_ptr<std_srvs::srv::Empty::Request> r1,
                                                                    std::shared_ptr<std_srvs::srv::Empty::Response> r2)
                                                             { startExperiment(r1, r2); } );

    // Create subscriptions for robot state
    odomPlatformSubscription = create_subscription<nav_msgs::msg::Odometry>("/FPR/Mocap/odom",
                                                                          qos_sensor_fpr,
                                                                          [this](nav_msgs::msg::Odometry::SharedPtr PH1) {
                                                                              updatePlatformPose(PH1); });
    for (int i=0; i<NUM_LEG; i++)
        odomDroneSubscription[i] = create_subscription<nav_msgs::msg::Odometry>(
                "/Drone" + std::to_string(droneIndexes[i]) + "/EKF/odom",
                qos_sensor_fpr,
                [this, i](nav_msgs::msg::Odometry::SharedPtr PH1) {
                    updateDronePose(PH1, i);
                });

    // Creat client to start attitude thrust control
    for (int i=0; i<NUM_LEG; i++)
        startControlClient[i] = create_client<ls2n_interfaces::srv::StartControl>("/Drone"+std::to_string(droneIndexes[i])+"/StartControl");

    // Create publisher for drone command
    for (int i=0; i<NUM_LEG; i++)
        attitudeThrustPublishDrone[i] = create_publisher<ls2n_interfaces::msg::AttitudeThrustSetPoint>("/Drone"+std::to_string(droneIndexes[i])+"/AttitudeThrustSetPoint", qos_sensor_fpr);

    if(!get_parameter("enable_rviz", broadcast_frames))
        broadcast_frames = false;
    else
        tfBroadcaster = std::make_shared<tf2_ros::TransformBroadcaster>(this);

    // Create the control loop
    timer = create_wall_timer(20ms, [this] { controlLoop(); });

    RCLCPP_INFO(get_logger(), "Fpr Autopilot Initialized");
}

void RosFprAutopilot::controlLoop() {
    checkDroneStatus();
    // Updates current pose
    poseFpr->getLegFromDrones(poseDrone);
    velocityFpr->getVelFromDrones(poseDrone, *poseFpr);
    controllerFpr->updateCurrentPose(*poseFpr);
    controllerFpr->updateCurrentVelocity(velocityFpr->curr_vel);
    if (status == DroneStatus::FLYING) {
        // Updates desired trajectory
        //Eigen::VectorXd traj_position(DIM_DOF+1);
        //Eigen::VectorXd traj_velocity(DIM_DOF);
        //Eigen::VectorXd traj_acceleration(DIM_DOF);
        //trajectoryFpr->getTrajNow(traj_position, traj_velocity, traj_acceleration, now().seconds());
        //controllerFpr->updateTrajectory(traj_position, traj_velocity, traj_acceleration);
        controllerFpr->updateTrajectory(trajFpr.pose, trajFpr.velocity, trajFpr.acceleration);
        // Computes the controller
        std::string err;
        controllerFpr->spinController(err);
        if (!err.empty())
            RCLCPP_ERROR(get_logger(), ("Controller error: "+ err).c_str());
        // Send command to the drones
        std::vector<FPR::ControllerFPR::DroneTarget> droneTarget;
        controllerFpr->getDroneTargets(droneTarget);
        for (int i = 0; i<3; i++)
        {
            auto msg = ls2n_interfaces::msg::AttitudeThrustSetPoint();
            msg.set__attitude(std::array<double,4>({droneTarget[i].attitude.w(),
                                droneTarget[i].attitude.x(),
                                droneTarget[i].attitude.y(),
                                droneTarget[i].attitude.z()}));
            msg.set__thrust(droneTarget[i].thrust);
            attitudeThrustPublishDrone[i]->publish(msg);
        }
    }
    else
    {
        for (int i = 0; i<3; i++)
        {
            auto msg = ls2n_interfaces::msg::AttitudeThrustSetPoint();
            msg.set__attitude(std::array<double,4>({poseDrone[i].attitude.w(),
                                                    poseDrone[i].attitude.x(),
                                                    poseDrone[i].attitude.y(),
                                                    poseDrone[i].attitude.z()}));
            msg.set__thrust(0.0);
            attitudeThrustPublishDrone[i]->publish(msg);
        }
    }

    if (broadcast_frames)
        broadcastFrames();
}

void RosFprAutopilot::transferKeepAlive(std_msgs::msg::Bool::SharedPtr msg) {
    auto msg_send = std_msgs::msg::Bool();
    if (status == DroneStatus::EMERGENCY_STOP)
        msg_send.data = false;
    else
        msg_send.data = msg->data;
    for (const auto &publisher: keepAliveDronePublisher)
        publisher->publish(msg_send);
}

void RosFprAutopilot::spinMotors(const std::shared_ptr<std_srvs::srv::Empty::Request>,
                                std::shared_ptr<std_srvs::srv::Empty::Response>) {
    if (status == DroneStatus::IDLE) {
        RCLCPP_INFO(get_logger(), "Spinning FPR motors");
        auto request = std::make_shared<std_srvs::srv::Empty::Request>();
        for (const auto &client: spinMotorsClient)
            client->async_send_request(request);
        }
    else
        RCLCPP_ERROR(get_logger(), "Request refused: Not IDLE");
}

void RosFprAutopilot::startExperiment(const std::shared_ptr<std_srvs::srv::Empty::Request>,
                                      std::shared_ptr<std_srvs::srv::Empty::Response>) {
    if (status == DroneStatus::ARMED){
        RCLCPP_INFO(get_logger(), "Starting experiment");
        auto request = std::make_shared<ls2n_interfaces::srv::StartControl::Request>();
        request->control_mode = ls2n_interfaces::srv::StartControl::Request::ATTITUDE_THRUST;
        for(const auto &client:startControlClient)
            client->async_send_request(request);
    }
    else
        RCLCPP_ERROR(get_logger(), "Request refused: Not ARMED");
}

void RosFprAutopilot::startFlying() {
    // Get the initial position and time and start controller
    /*trajectoryFpr->setInitPose(poseFpr->platform_position, poseFpr->platform_orientation, poseFpr->angle_leg);
    trajectoryFpr->setInitTime(now().seconds());*/
    std::cout << "Initial pose: " << std::endl;
    std::cout << "  Platform position: " << poseFpr->platform_position << std::endl;
    std::cout << "  Platform orientation: " << poseFpr->platform_orientation.vec() << " " << poseFpr->platform_orientation.w() << std::endl;
    std::cout << "  Leg angles: " << poseFpr->leg_angles << std::endl;
    initPoseFpr->setPose(*poseFpr);
    status = DroneStatus::FLYING;
    RCLCPP_INFO(get_logger(), "Flying");
}

void RosFprAutopilot::updateDroneStatus(ls2n_interfaces::msg::DroneStatus::SharedPtr msg, const int& index) {
    droneStatus[index] = msg->status;
}

void RosFprAutopilot::checkDroneStatus() {
    auto allDrones = [this](int8_t s_test){return std::all_of(droneStatus.begin(), droneStatus.end(), [s_test](int8_t s){return s == s_test;});};
    auto anyDrone = [this](int8_t s_test){return std::any_of(droneStatus.begin(), droneStatus.end(), [s_test](int8_t s){return s == s_test;});};
    // Updates the status of the fpr node as function of the current status and the drones status
    if (status == DroneStatus::IDLE && allDrones(DroneStatus::ARMED)) {
        status = DroneStatus::ARMED;
        RCLCPP_INFO(get_logger(), "Drones are armed");
    }
    if (status == DroneStatus::ARMED && allDrones(DroneStatus::FLYING))
        startFlying();
    if (anyDrone(DroneStatus::EMERGENCY_STOP) && status != DroneStatus::EMERGENCY_STOP && status != DroneStatus::IDLE){
        status = DroneStatus::EMERGENCY_STOP;
        RCLCPP_INFO(get_logger(), "Emergency stop");
    }
    if (status == DroneStatus::FLYING){
        if (!allDrones(DroneStatus::FLYING)){
            status = DroneStatus::EMERGENCY_STOP;
            RCLCPP_INFO(get_logger(), "Emergency stop");
        }
    }
}

void RosFprAutopilot::updateDronePose(nav_msgs::msg::Odometry::SharedPtr msg, const int& index) {
    poseDrone[index].timestamp = rclcpp::Time(msg->header.stamp).seconds();
    poseDrone[index].position.x() = msg->pose.pose.position.x;
    poseDrone[index].position.y() = msg->pose.pose.position.y;
    poseDrone[index].position.z() = msg->pose.pose.position.z;
    poseDrone[index].attitude.x() = msg->pose.pose.orientation.x;
    poseDrone[index].attitude.y() = msg->pose.pose.orientation.y;
    poseDrone[index].attitude.z() = msg->pose.pose.orientation.z;
    poseDrone[index].attitude.w() = msg->pose.pose.orientation.w;
    poseDrone[index].linear_velocity.x() = msg->twist.twist.linear.x;
    poseDrone[index].linear_velocity.y() = msg->twist.twist.linear.y;
    poseDrone[index].linear_velocity.z() = msg->twist.twist.linear.z;
}

void RosFprAutopilot::updatePlatformPose(nav_msgs::msg::Odometry::SharedPtr msg) {
    poseFpr->platform_position.x() = msg->pose.pose.position.x;
    poseFpr->platform_position.y() = msg->pose.pose.position.y;
    poseFpr->platform_position.z() = msg->pose.pose.position.z;
    poseFpr->platform_orientation.x() = msg->pose.pose.orientation.x;
    poseFpr->platform_orientation.y() = msg->pose.pose.orientation.y;
    poseFpr->platform_orientation.z() = msg->pose.pose.orientation.z;
    poseFpr->platform_orientation.w() = msg->pose.pose.orientation.w;
}

void RosFprAutopilot::updateTrajectory(ls2n_fpr_interfaces::msg::FprTrajectory::SharedPtr traj)
{
    double dt = (now() - traj->header.stamp).seconds();
    if(abs(dt) > 0.5 ) // greater than 0.5 sec
    {
        RCLCPP_WARN(get_logger(), "Received trajectory is out of time");
    }
    trajFpr.pose(0) = traj->platform_pose.position.x;
    trajFpr.pose(1) = traj->platform_pose.position.y;
    trajFpr.pose(2) = traj->platform_pose.position.z;
    trajFpr.pose(3) = traj->platform_pose.orientation.x;
    trajFpr.pose(4) = traj->platform_pose.orientation.y;
    trajFpr.pose(5) = traj->platform_pose.orientation.z;
    trajFpr.pose(6) = traj->platform_pose.orientation.w;
    trajFpr.velocity(0) = traj->platform_twist.linear.x;
    trajFpr.velocity(1) = traj->platform_twist.linear.y;
    trajFpr.velocity(2) = traj->platform_twist.linear.z;
    trajFpr.velocity(3) = traj->platform_twist.angular.x;
    trajFpr.velocity(4) = traj->platform_twist.angular.y;
    trajFpr.velocity(5) = traj->platform_twist.angular.z;
    trajFpr.acceleration(0) = traj->platform_accel.linear.x;
    trajFpr.acceleration(1) = traj->platform_accel.linear.y;
    trajFpr.acceleration(2) = traj->platform_accel.linear.z;
    trajFpr.acceleration(3) = traj->platform_accel.angular.x;
    trajFpr.acceleration(4) = traj->platform_accel.angular.y;
    trajFpr.acceleration(5) = traj->platform_accel.angular.z;
    for(int i=0; i<NUM_LEG; i++)
    {
        trajFpr.pose(7+i) = traj->leg_angles.at(i);
        trajFpr.velocity(6+i) = traj->leg_angles_vel.at(i);
        trajFpr.acceleration(6+i) = traj->leg_angles_accel.at(i);
    }
    // adding initial platform position in the desired trajectory
    trajFpr.pose.segment(0,3) += initPoseFpr->platform_position;
}

void RosFprAutopilot::broadcastFrames() {
    std::vector<geometry_msgs::msg::TransformStamped> tf_msg;
    // Platform frame
    FramesFPR::set_frame(framesFpr.platform_frame, 
            poseFpr->platform_position, 
            poseFpr->platform_orientation, 
            "Platform");
    framesFpr.platform_frame.header.stamp = get_clock()->now();
    tf_msg.push_back(framesFpr.platform_frame);
    // Legs and drones frames
    Eigen::Matrix3d Rp = poseFpr->platform_orientation.toRotationMatrix();
    for(int i=0; i < NUM_LEG; i++)
    {
        Eigen::Vector3d p_leg = poseFpr->platform_position + Rp*paramFpr.pos_ri[i]; // position of leg i's frame expressed in frame 0
        Eigen::Matrix3d Rleg = buildUnitRotMat(paramFpr.alpha[i],'z')*buildUnitRotMat(-M_PI/2,'x')*buildUnitRotMat(poseFpr->leg_angles(i),'z');
        Eigen::Quaterniond q_leg(Rp*Rleg);        
        FramesFPR::set_frame(framesFpr.leg_frames[i], 
            p_leg, q_leg, 
            "Leg"+std::to_string(i+1));
        FramesFPR::set_frame(framesFpr.drone_frames[i], 
            poseDrone[i].position, poseDrone[i].attitude, 
            "Drone"+std::to_string(droneIndexes[i]));
        framesFpr.leg_frames[i].header.stamp = get_clock()->now();
        framesFpr.drone_frames[i].header.stamp = get_clock()->now();
        tf_msg.push_back(framesFpr.leg_frames[i]);
        tf_msg.push_back(framesFpr.drone_frames[i]);
    }
    // Broadcast frames
    tfBroadcaster->sendTransform(tf_msg);
}

bool RosFprAutopilot::loadParams() {
    std::string ctrl_type;
    if(!get_parameter("controller_type", ctrl_type))
    {
        RCLCPP_ERROR(get_logger(), "ControllerType not defined, controller_type parameter is missing");
        return false;
    }
    else
    {
        if(strcasecmp(ctrl_type.c_str(), "pd")==0)
            controllerType = ControllerType::PD_CTC;
        else if(strcasecmp(ctrl_type.c_str(), "pid")==0)
            controllerType = ControllerType::PID_CTC;
        else if(strcasecmp(ctrl_type.c_str(), "impedance")==0)
            controllerType = ControllerType::IMPEDANCE;
        else
        {
            RCLCPP_ERROR(get_logger(), "ControllerType not defined, controller_type parameter is invalid");
            return false;
        }
    }

    bool loaded = get_parameter("geometric_param.l_leg", paramFpr.l_leg) &
        get_parameter("geometric_param.r_p", paramFpr.r_p) &
        get_parameter("dynamic_param.mass_p", paramFpr.mp) &
        get_parameter("dynamic_param.sx_p", paramFpr.sp.x()) &
        get_parameter("dynamic_param.sy_p", paramFpr.sp.y()) &
        get_parameter("dynamic_param.sz_p", paramFpr.sp.z()) &
        get_parameter("dynamic_param.mass_leg", paramFpr.mleg) &
        get_parameter("dynamic_param.sx_leg", paramFpr.sleg.x()) &
        get_parameter("dynamic_param.sy_leg", paramFpr.sleg.y()) &
        get_parameter("dynamic_param.sz_leg", paramFpr.sleg.z()) &
        get_parameter("dynamic_param.xx_p", paramFpr.Ip(0,0)) &
        get_parameter("dynamic_param.xy_p", paramFpr.Ip(0,1)) &
        get_parameter("dynamic_param.xz_p", paramFpr.Ip(0,2)) &
        get_parameter("dynamic_param.yz_p", paramFpr.Ip(1,2)) &
        get_parameter("dynamic_param.yy_p", paramFpr.Ip(1,1)) &
        get_parameter("dynamic_param.zz_p", paramFpr.Ip(2,2)) &
        get_parameter("dynamic_param.xx_leg", paramFpr.Ileg(0,0)) &
        get_parameter("dynamic_param.xy_leg", paramFpr.Ileg(0,1)) &
        get_parameter("dynamic_param.xz_leg", paramFpr.Ileg(0,2)) &
        get_parameter("dynamic_param.yz_leg", paramFpr.Ileg(1,2)) &
        get_parameter("dynamic_param.yy_leg", paramFpr.Ileg(1,1)) &
        get_parameter("dynamic_param.zz_leg", paramFpr.Ileg(2,2)) &
        get_parameter("drone_param.drone0.num", droneIndexes[0]) &
        get_parameter("drone_param.drone1.num", droneIndexes[1]) &
        get_parameter("drone_param.drone2.num", droneIndexes[2]) &
        get_parameter("drone_param.drone0.angle", paramFpr.beta[0]) &
        get_parameter("drone_param.drone1.angle", paramFpr.beta[1]) &
        get_parameter("drone_param.drone2.angle", paramFpr.beta[2]) &
        get_parameter("drone_param.drone0.mass", paramFpr.m_drones[0]) & // TODO get it from the drones (ROS service)
        get_parameter("drone_param.drone1.mass", paramFpr.m_drones[1]) &
        get_parameter("drone_param.drone2.mass", paramFpr.m_drones[2]);

    if (controllerType == ControllerType::PD_CTC || controllerType == ControllerType::PID_CTC)
    {
        loaded = loaded & 
            get_parameter("control_gain.position.kp_xy", paramFpr.ctrl_gain.kp_p[0]) &
            get_parameter("control_gain.position.kp_xy", paramFpr.ctrl_gain.kp_p[1]) &
            get_parameter("control_gain.position.kp_z", paramFpr.ctrl_gain.kp_p[2]) &
            get_parameter("control_gain.position.kd_xy", paramFpr.ctrl_gain.kd_p[0]) &
            get_parameter("control_gain.position.kd_xy", paramFpr.ctrl_gain.kd_p[1]) &
            get_parameter("control_gain.position.kd_z", paramFpr.ctrl_gain.kd_p[2]) &
            get_parameter("control_gain.orientation.kp_xy", paramFpr.ctrl_gain.kp_o[0]) &
            get_parameter("control_gain.orientation.kp_xy", paramFpr.ctrl_gain.kp_o[1]) &
            get_parameter("control_gain.orientation.kp_z", paramFpr.ctrl_gain.kp_o[2]) &
            get_parameter("control_gain.orientation.kd_xy", paramFpr.ctrl_gain.kd_o[0]) &
            get_parameter("control_gain.orientation.kd_xy", paramFpr.ctrl_gain.kd_o[1]) &
            get_parameter("control_gain.orientation.kd_z", paramFpr.ctrl_gain.kd_o[2]) &
            get_parameter("control_gain.leg_angle.kp_l", paramFpr.ctrl_gain.kp_l) &
            get_parameter("control_gain.leg_angle.kd_l", paramFpr.ctrl_gain.kd_l) &
            get_parameter("control_gain.position.ki_xy", paramFpr.ctrl_gain.ki_p[0]) &
            get_parameter("control_gain.position.ki_xy", paramFpr.ctrl_gain.ki_p[1]) &
            get_parameter("control_gain.position.ki_z", paramFpr.ctrl_gain.ki_p[2]) &      
            get_parameter("control_gain.orientation.ki_xy", paramFpr.ctrl_gain.ki_o[0]) &
            get_parameter("control_gain.orientation.ki_xy", paramFpr.ctrl_gain.ki_o[1]) &
            get_parameter("control_gain.orientation.ki_z", paramFpr.ctrl_gain.ki_o[2]) &
            get_parameter("control_gain.leg_angle.ki_l", paramFpr.ctrl_gain.ki_l);
    }
    if (controllerType == ControllerType::IMPEDANCE)
    {
        loaded = loaded & 
            get_parameter("control_gain.impedance.mv_p", paramFpr.ctrl_gain.mv_p) &
            get_parameter("control_gain.impedance.dv_p", paramFpr.ctrl_gain.dv_p) &
            get_parameter("control_gain.impedance.kv_p", paramFpr.ctrl_gain.kv_p) &
            get_parameter("control_gain.impedance.mv_o", paramFpr.ctrl_gain.mv_o) &
            get_parameter("control_gain.impedance.dv_o", paramFpr.ctrl_gain.dv_o) &
            get_parameter("control_gain.impedance.kv_o", paramFpr.ctrl_gain.kv_o) &
            get_parameter("control_gain.impedance.mv_l", paramFpr.ctrl_gain.mv_l) &
            get_parameter("control_gain.impedance.dv_l", paramFpr.ctrl_gain.dv_l) &
            get_parameter("control_gain.impedance.kv_l", paramFpr.ctrl_gain.kv_l);
    }
    return loaded;
}

bool RosFprAutopilot::trajectoryInit() {
    std::string fileName;
    std::string err;
    std::string pkg_share_dir;
    try{
        pkg_share_dir = ament_index_cpp::get_package_share_directory("ls2n_fpr_control");
    }
    catch (ament_index_cpp::PackageNotFoundError& e){
        RCLCPP_ERROR(get_logger(), "Load trajectory error: Package not found");
    }
    if (get_parameter("trajectory_param.file", fileName))
        if (trajectoryFpr->loadWaypointFromTxtFile(pkg_share_dir+"/config/traj/"+fileName, err))
            if (trajectoryFpr->interpolateTraj(err))
                return true;

    RCLCPP_ERROR(get_logger(), ("Load trajectory error:" + err).c_str());
    return false;
}

int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);
    rclcpp::spin(std::make_shared<RosFprAutopilot>());
    rclcpp::shutdown();
    return 0;
}