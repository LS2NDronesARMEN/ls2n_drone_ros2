#include "trajectory_fpr.h"
using namespace FPR;

TrajectoryFPR::TrajectoryFPR(TrajectoryType _type): TrajectoryBase(_type)
{ 
    n_dof = data_size-1; 
    
    init_position.setZero();
    init_orientation.setIdentity();
    init_angle.setZero();
}

bool TrajectoryFPR::interpolateTraj(std::string& msgerr)
{
    return doInterpolation(msgerr);
}

void TrajectoryFPR::getTrajNow(Eigen::VectorXd& _pos, Eigen::VectorXd& _vel, Eigen::VectorXd& _acc, const double _tNow)
{
    Eigen::VectorXd raw_pos; raw_pos.setZero(state_size); // raw position data
    Eigen::VectorXd raw_vel; raw_vel.setZero(state_size); //raw velocity (corresponding to state derivatives)
    Eigen::VectorXd raw_acc; raw_acc.setZero(state_size); //raw acceleration (idem)

    getInterpValue(raw_pos, raw_vel, raw_acc, _tNow);
    raw_pos.segment(0,3) += init_position;
    //raw_pos.segment(7,NUM_LEG) += init_angle;

    if(_tNow > t_fin)
    {
        _pos = raw_pos;
        _vel = Deriv_Type::Zero();
        _acc = Deriv_Type::Zero();
        return;
    }

    Eigen::Quaterniond quat; quat = Eigen::Vector4d(raw_pos.segment(3,4)); // orientation of the platform represented by quaternion
    Eigen::Quaterniond quat_dot; quat_dot = Eigen::Vector4d(raw_vel.segment(3,4)); // derivatives of quaternion
    Eigen::Quaterniond quat_ddot; quat_ddot = Eigen::Vector4d(raw_acc.segment(3,4));

    if(quat.norm() != 1) // normalize quternion
        quat.normalize();

    /* computation of platform's angular velocity/acceleration based on quaternion and its derivatives */
    Eigen::Quaterniond quat_conj = quat.conjugate();
    quat_conj.w()*=2.0; quat_conj.vec()*=2.0;
    // quat_dot = 1/2* quat (*) w   =>   w = 2* (quat_conj (*) quat_dot)   (*): represents quaternion multiplication
    Eigen::Quaterniond w = quat_conj*quat_dot; // w = [0; omega] where omega is angular velocity expressed in body frame

    // w_dot = 2*(quat_conj (*) quat_ddot + norm2(quat_dot))
    Eigen::Quaterniond w_dot = quat_conj*quat_ddot;
    w_dot.w() += 2.0*quat_dot.squaredNorm(); // w_dot = [0; omega_dot] angular acceleration
    
    /* outputs: */
    // position:
    _pos << raw_pos.segment(0,3),   // platform's COM position
            quat.vec(), quat.w(),   // platform's orientation (be careful of order -> quaternion = [qx, qy, qz, qw])
            raw_pos.segment(7,NUM_LEG);   // angles of revolute joints
    // velocity:
    _vel << raw_vel.segment(0,3), w.vec(), raw_vel.segment(7,NUM_LEG);

    // acceleration:
    _acc << raw_acc.segment(0,3), w_dot.vec(), raw_acc.segment(7,NUM_LEG);
    return;
}

void TrajectoryFPR::setInitPose(Eigen::Vector3d _init_pos, Eigen::Quaterniond _init_orient, VectorNdFPR _init_ang)
{
    init_position = _init_pos;
    init_orientation = _init_orient;
    init_angle = _init_ang;

    waypoints.front().segment(4,4) << _init_orient.vec(), _init_orient.w();
    waypoints.front().segment(8,NUM_LEG) << _init_ang;
}
