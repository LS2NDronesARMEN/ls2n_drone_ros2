#include "model_fpr.h"

using namespace FPR;

Eigen::Matrix<double, DIM_DOF, 1> recursiveNewtonEuler(const double _g, const Eigen::Matrix<double, DIM_DOF, 1>& _vel, const Eigen::Matrix<double, DIM_DOF, 1>& _acc, 
        const ParamFPR _pm, const Eigen::Matrix3d& _Rp, Eigen::Matrix3d* _p_Rleg)
/*  Dynamic modeling based on recursive Newton-Euler method, 
reference: https://link.springer.com/book/10.1007/978-3-319-19788-3. */
{
    // return of a 9-dimensional vector
    Eigen::Matrix<double, DIM_DOF, 1> ret; ret.setZero();
    
    Eigen::Vector3d acc_p; acc_p << _acc.segment(0,3); // linear acceleration of platform w.r.t frame F0
    Eigen::Vector3d omega_p; omega_p << _vel.segment(3,3); // angular velocity of platform w.r.t frame Fp
    Eigen::Vector3d domega_p; domega_p << _acc.segment(3,3); // angular acceleration of platform w.r.t frame Fp
    
    Eigen::Vector3d acc_leg[NUM_LEG];
    Eigen::Vector3d omega_leg[NUM_LEG]; 
    Eigen::Vector3d domega_leg[NUM_LEG];

    acc_p(2) += _g; // adding gravity
    acc_p = _Rp.transpose()*acc_p; // linear acc of platform w.r.t its frame Fp

    // forward recursive:
    for (int i=0; i < NUM_LEG; i++) // computation of velocity and acceleration of leg frame
    {
        Eigen::Vector3d ri_i = _pm.pos_ri[i];
        Eigen::Matrix3d Rli2p = (*(_p_Rleg+i)).transpose(); // rotation matrix from frame Fli to Fp
        omega_leg[i] = Rli2p*omega_p + Eigen::Vector3d(0,0,_vel(6+i));
        domega_leg[i] = Rli2p*domega_p + Eigen::Vector3d(0,0,_acc(6+i)) + omega_leg[i].cross(Eigen::Vector3d(0,0,_vel(6+i)));
        // angular velocity and acceleration of leg w.r.t its frame Fli
        acc_leg[i] = Rli2p*(acc_p + domega_p.cross(ri_i) + omega_p.cross(omega_p.cross(ri_i)));
        // linear acceleration of leg w.r.t its frame Fli
    }

    // sum of force and torque on each body
    Eigen::Vector3d sum_force_p = _pm.mp*acc_p + domega_p.cross(_pm.ms_p) + omega_p.cross(omega_p.cross(_pm.ms_p));
    Eigen::Vector3d sum_torque_p = _pm.Ip*domega_p + _pm.ms_p.cross(acc_p) + omega_p.cross(_pm.Ip*omega_p);
    
    Eigen::Vector3d sum_force_leg[NUM_LEG];
    Eigen::Vector3d sum_torque_leg[NUM_LEG];
    for (int i=0; i < NUM_LEG; i++)
    {
        sum_force_leg[i] = _pm.m_leg[i]*acc_leg[i] + domega_leg[i].cross(_pm.ms_leg[i]) + omega_leg[i].cross(omega_leg[i].cross(_pm.ms_leg[i]));
        sum_torque_leg[i] = _pm.I_leg[i]*domega_leg[i] + _pm.ms_leg[i].cross(acc_leg[i]) + omega_leg[i].cross(_pm.I_leg[i]*omega_leg[i]);
    }

    Eigen::Vector3d force_p = sum_force_p;
    Eigen::Vector3d torque_p = sum_torque_p;
    // backwards recursive:
    for (int i=0; i < NUM_LEG; i++)
    {   
        Eigen::Vector3d ri_i = _pm.pos_ri[i];
        Eigen::Matrix3d Rli = *(_p_Rleg+i); // rotation matrix from Fp to Fli
        Eigen::Vector3d Fi_p = Rli*sum_force_leg[i]; // force of leg i expressed in frame Fp
        force_p += Fi_p;
        torque_p += Rli*sum_torque_leg[i] + (ri_i).cross(Fi_p);
        ret(6+i) = sum_torque_leg[i](2); // z-axis torque of each leg (around axis of the revolute joint)
    }
    ret.segment(0,3) = _Rp*force_p;
    ret.segment(3,3) = torque_p;

    return ret;
}


void ModelFPR::computeDynamicModel(const Eigen::Matrix<double, DIM_DOF+1, 1>& current_pos, const Eigen::Matrix<double, DIM_DOF, 1>& current_vel)
// Computation of the dynamic model (inertia matrix and coriolis vector) of FPR depending on the current position and veloctiy, the kinematic model is also computed (Jacobian matrix)
{
// frames: inertial reference F0, platform frame Fp, leg i's frame Fli (its origin located at the center of revolute joint)

    Eigen::Quaterniond orient;
    orient = Eigen::Vector4d(current_pos.segment(3,4));
    Eigen::Vector3d qi = current_pos.segment(7,NUM_LEG);

    Rp = orient.toRotationMatrix();

    for (int i=0; i < NUM_LEG; i++)
    {
        Eigen::Vector3d ri_i = param.pos_ri[i];
        Eigen::Matrix3d Rleg_i = buildUnitRotMat(param.alpha[i], 'z')*buildUnitRotMat(-M_PI/2,'x')*buildUnitRotMat(qi(i),'z'); 
        
        // computation of jacobian matrix
        int index = 3*i;
        JacobianMat.block<3,3>(index,0)= Eigen::Matrix3d::Identity();

        Eigen::Vector3d ui_i = Rleg_i*Eigen::Vector3d(param.l_leg,0,0); // leg's tip position expressed in frame Fp
        JacobianMat.block<3,3>(index,3)= -Rp*(skewSymMatrix(ri_i)+skewSymMatrix(ui_i));
        
        Eigen::Vector3d y_li = (Rp*Rleg_i).col(1); // y-axis of the rotation matrix from frame F0 to Fli
        JacobianMat.block<3,1>(index,6+i)= param.l_leg*y_li;

        Rleg[i]= Rleg_i;
    }

    for (int i=0; i<9; i++)
    {
        Eigen::Matrix<double, DIM_DOF, 1> acc; acc.setZero();
        acc(i) = 1;

        // computation of inertia matrix column by column
        InertiaMat.col(i) = recursiveNewtonEuler(0, Eigen::VectorXd::Zero(DIM_DOF), acc, param, Rp, Rleg);
    }

    // computation of coriolis vector
    CoriolisVec = recursiveNewtonEuler(GRAVITY, current_vel, Eigen::VectorXd::Zero(DIM_DOF), param, Rp, Rleg);
}


Eigen::Matrix<double, DIM_ACT, DIM_DOF> ModelFPR::computeKinematicModel(const Eigen::Matrix<double, DIM_DOF+1, 1>& current_pos)
// Computation of kinematic model (Jacobian matrix)
{
    Eigen::Quaterniond orient; 
    orient = Eigen::Vector4d(current_pos.segment(3,4));
    Eigen::Vector3d qi = current_pos.segment(7,NUM_LEG);

    Rp = orient.toRotationMatrix();

    for (int i=0; i < NUM_LEG; i++)
    {
        Eigen::Vector3d ri_i = param.pos_ri[i];
        Eigen::Matrix3d Rleg_i = buildUnitRotMat(param.alpha[i], 'z')*buildUnitRotMat(-M_PI/2,'x')*buildUnitRotMat(qi(i),'z'); 
        
        // computation of jacobian matrix
        int index = 3*i;
        JacobianMat.block<3,3>(index,0)= Eigen::Matrix3d::Identity();

        Eigen::Vector3d ui_i = Rleg_i*Eigen::Vector3d(param.l_leg,0,0); // leg's tip position expressed in frame Fp
        JacobianMat.block<3,3>(index,3)= -Rp*(skewSymMatrix(ri_i)+skewSymMatrix(ui_i));
        
        Eigen::Vector3d y_li = (Rp*Rleg_i).col(1); // y-axis of the rotation matrix from frame F0 to Fli
        JacobianMat.block<3,1>(index,6+i)= param.l_leg*y_li;
    }
    return JacobianMat;
}