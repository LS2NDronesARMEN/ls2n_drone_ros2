from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import ExecuteProcess
from launch.actions import OpaqueFunction
from ament_index_python.packages import get_package_share_directory
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration
import os
import yaml
from datetime import datetime

now = datetime.now()
dt_string = now.strftime("%d-%m-%Y_%H:%M:%S")


# Set the bridge parameters according to the drone type for simulation
def get_bridge_parameters(drone_type):
    mass = 1.0
    max_thrust = 46.0
    kp = 3.0
    kd = 1.5
    ki = 0.5
    if drone_type == 'crazyflie2':
        mass = 0.035
        max_thrust = 0.56
        kp = 5.0
        kd = 1.5
        ki = 0.5
    return {'mass': mass, 'max_thrust': max_thrust, 'kp': kp, 'kd': kd, 'ki': ki}


def launch_setup(context, *args, **kwargs):
    config_file = os.path.join(
        get_package_share_directory('ls2n_drone_simulation'),
        'config',
        LaunchConfiguration('param_file').perform(context)
    )
    with open(config_file) as file:
        config = yaml.load(file, Loader=yaml.FullLoader)

    px4_folder_file = os.path.join(
            get_package_share_directory('ls2n_drone_simulation'),
            'config',
            'simu_folders.yaml'
        )
    with open(px4_folder_file) as file:
        folders_config = yaml.load(file, Loader=yaml.FullLoader)

    model = config['ls2n_drone_simulation']['launch_parameters']['model']+"_rtps"
    px4_folder = folders_config['ls2n_drone_simulation']['launch_parameters']['px4_folder']
    log_folder = folders_config['ls2n_drone_simulation']['launch_parameters']['logs_folder']
    n_drones = config['ls2n_drone_simulation']['launch_parameters']['number_of_drones']
    gz_model_path = os.path.join(get_package_share_directory('ls2n_drone_simulation'), 'models')
    gz_plugin_path = os.getenv("HOME")+"/.ls2n_gz_plugins"

    # One bridge node per drone is started
    start_bridge_nodes = [Node(
        package='ls2n_drone_bridge', executable='px4_rtps_bridge',
        output='screen', namespace='Drone'+str(i+1),
        parameters=[get_bridge_parameters(config['ls2n_drone_simulation']['launch_parameters']['model'])])
                          for i in range(n_drones)]

    return [
        # PX4 script to spawn the drones (firmware sitl + gazebo)
        ExecuteProcess(
            cmd=[px4_folder+'/Tools/gazebo_sitl_multiple_run.sh', '-m',
                 model, '-n', str(n_drones), '-t', 'px4_sitl_rtps',
                 '-w', LaunchConfiguration('gz_world').perform(context),
                 '-a', gz_plugin_path,
                 '-b', gz_model_path]
        ),
        # Record the bags as on the offboard node
        ExecuteProcess(
           cmd=['ros2', 'bag', 'record', '-o', log_folder + "/" + dt_string,
                '-e', '.*PubSubTopic$'],
           output='screen'
        )] + start_bridge_nodes


def generate_launch_description():
    return LaunchDescription([
        DeclareLaunchArgument('param_file', default_value='single_drone_params.yaml',
                              description='Specify the parameter file'),
        DeclareLaunchArgument('gz_world', default_value='empty',
                              description='Gazebo world to use'),
        OpaqueFunction(function=launch_setup)
    ])
