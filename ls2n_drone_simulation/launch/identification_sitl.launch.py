from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import ThisLaunchFileDir
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory


def generate_launch_description():
    return LaunchDescription([
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([ThisLaunchFileDir(), '/drones_sitl.launch.py']),
            launch_arguments={'param_file': 'single_drone_params.yaml',
                              'gz_world': 'empty',
                              'direct_control_mode': 'none'}.items()
        ),
        Node(package='ls2n_drone_command_center', executable='fake_joystick',
             output='screen', namespace='CommandCenter'),
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([
                get_package_share_directory('ls2n_drone_support'), '/identification.launch.py'
            ]),
            launch_arguments={
                "drone_namespace": "Drone1",
                "esc_feedback": "False"
            }.items()
        )
    ])
