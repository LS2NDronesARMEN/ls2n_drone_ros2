from launch import LaunchDescription
from ament_index_python.packages import get_package_share_directory
import os
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import ThisLaunchFileDir
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([ThisLaunchFileDir(), '/drones_sitl.launch.py']),
            launch_arguments={'param_file': 'fpr_params.yaml',
                              'gz_world': os.path.join(
                                  get_package_share_directory('ls2n_drone_simulation'),
                                  'worlds', 'fpr.world')}.items()
        ),
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([get_package_share_directory('ls2n_fpr_control'),
                                          '/launch/autopilot_fpr_simu.launch.py'])
        ),
        Node(package='ls2n_drone_command_center', executable='fake_joystick',
             output='screen', namespace='CommandCenter')
    ])
