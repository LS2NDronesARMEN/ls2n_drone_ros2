from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import ThisLaunchFileDir
from launch_ros.actions import Node


def generate_launch_description():
    return LaunchDescription([
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([ThisLaunchFileDir(), '/drones_sitl.launch.py']),
            launch_arguments={'param_file': 'single_drone_params.yaml',
                              'gz_world': 'empty'}.items()
        ),
        Node(package='ls2n_drone_command_center', executable='fake_joystick',
             output='screen', namespace='Drone1'),
        Node(package='ls2n_drone_support', executable='position_pid_tuning',
             output='screen', namespace='Drone1'),
    ])
